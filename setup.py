#! /usr/bin/env python
#
'''
   Setup script for ANTS-Hub
'''

import glob
import os
import subprocess
import sys

try:
    from setuptools import Command
    from setuptools.command.build import build as _build
except ImportError:
    from distutils.cmd import Command
    from distutils.command.build import build as _build

from setuptools import find_packages, setup
from setuptools.command.install_lib import install_lib as _install_lib
from setuptools.command.sdist import sdist as _sdist


class compile_translations(Command):
    description = 'compile message catalogs to MO files via django compilemessages'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        curdir = os.getcwd()
        try:
            os.environ.pop('DJANGO_SETTINGS_MODULE', None)
            from django.core.management import call_command

            for dir in glob.glob('src/*'):
                for path, dirs, files in os.walk(dir):
                    if 'locale' not in dirs:
                        continue
                    os.chdir(os.path.realpath(path))
                    call_command('compilemessages')
                    os.chdir(curdir)
        except ImportError:
            print
            sys.stderr.write('!!! Please install Django >= 1.4 to build translations')
            print
            print
            os.chdir(curdir)


class build(_build):
    sub_commands = [('compile_translations', None)] + _build.sub_commands


class sdist(_sdist):
    sub_commands = [('compile_translations', None)] + _sdist.sub_commands

    def run(self):
        print('creating VERSION file')
        if os.path.exists('VERSION'):
            os.remove('VERSION')
        version = get_version()
        version_file = open('VERSION', 'w')
        version_file.write(version)
        version_file.close()
        _sdist.run(self)
        print('removing VERSION file')
        if os.path.exists('VERSION'):
            os.remove('VERSION')


class install_lib(_install_lib):
    def run(self):
        self.run_command('compile_translations')
        _install_lib.run(self)


def get_version():
    """Use the VERSION, if absent generates a version with git describe, if not
    tag exists, take 0.0- and add the length of the commit log.
    """
    if os.path.exists('VERSION'):
        with open('VERSION') as v:
            return v.read()
    if os.path.exists('.git'):
        p = subprocess.Popen(
            ['git', 'describe', '--dirty=.dirty', '--match=v*'],
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
        )
        result = p.communicate()[0]
        if p.returncode == 0:
            result = result.decode('ascii').strip()[1:]  # strip spaces/newlines and initial v
            if '-' in result:  # not a tagged version
                real_number, commit_count, commit_hash = result.split('-', 2)
                version = '%s.post%s+%s' % (real_number, commit_count, commit_hash)
            elif result.endswith('.dirty'):
                version = result[:-6] + '+dirty'
            else:
                version = result
            return version
        else:
            return '0.0.post%s' % len(subprocess.check_output(['git', 'rev-list', 'HEAD']).splitlines())
    return '0.0'


with open('README') as fd:
    long_description = fd.read()

setup(
    name='ants-hub',
    version=get_version(),
    license='AGPLv3+',
    description='ANTS-Hub',
    long_description=long_description,
    url='http://dev.entrouvert.org/projects/ants-hub/',
    author="Entr'ouvert",
    maintainer='Benjamin Dauvergne',
    maintainer_email='bdauvergne@entrouvert.com',
    scripts=('manage.py',),
    packages=find_packages('src'),
    package_dir={
        '': 'src',
    },
    include_package_data=True,
    install_requires=[
        'django>=3.2,<3.3',
        'jsonschema',
        'requests',
    ],
    classifiers=[
        'License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
    cmdclass={
        'build': build,
        'install_lib': install_lib,
        'compile_translations': compile_translations,
        'sdist': sdist,
    },
)
