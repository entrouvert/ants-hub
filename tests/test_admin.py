# ANTS-Hub - Copyright (C) Entr'ouvert

import pytest
from django.core.management import call_command
from django.db import transaction


def table_text(table):
    content = []
    for row in table.find('tr').items():
        content.append([cell.text() for cell in (row.find('td, th')).items()])
    return content


@pytest.fixture(autouse=True, scope='module')
def setup(django_db_blocker):
    with django_db_blocker.unblock():
        with transaction.atomic():
            call_command('loaddata', 'fixtures/admin.json')
            call_command('loaddata', 'fixtures/example1.json')
            with django_db_blocker.block():
                yield
            transaction.set_rollback(True)


@pytest.fixture(autouse=True)
def freezer(freezer):
    freezer.move_to('2023-04-12T12:00:00+02:00')
    return freezer


def test_login(django_app, db):
    resp = django_app.get('/').maybe_follow()
    resp.form.set('username', 'admin')
    resp.form.set('password', 'admin')
    resp = resp.form.submit()


class TestLogged:
    @pytest.fixture
    def app(self, django_app, db):
        test_login(django_app, db)
        return django_app

    def test_create_raccordement(self, app):
        resp = app.get('/').maybe_follow()
        resp = resp.click('Ajouter', href='raccordement')
        resp.form.set('name', 'Plateforme Y')
        resp.form.set('apikey', 'a' * 32)
        resp.form.set('notes', 'Penser à facturer.')
        resp = resp.form.submit('_save').follow()
        assert table_text(resp.pyquery('#result_list')) == [
            ['', 'Nom', 'SETTING_CHRONO_ANTS_HUB_URL', 'Dernière mise à jour'],
            [
                '',
                'Plateforme X',
                'https://01a549058c1143ed98848ab79403ff99:@ants-hub.entrouvert.org/api/chrono/',
                '3 avril 2023 19:57',
            ],
            [
                '',
                'Plateforme Y',
                'https://aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa:@ants-hub.entrouvert.org/api/chrono/',
                '12 avril 2023 12:00',
            ],
        ]
        resp = resp.click('Plateforme Y', index=0)
        assert f'http://{"a" * 32}:@testserver/api/chrono/' in resp

    def test_collectivite(self, app):
        resp = app.get('/').maybe_follow()
        resp = resp.click('Collectivités')
        assert table_text(resp.pyquery('#result_list')) == [
            [
                '',
                'Raccordement',
                'Nom',
                'URL du portail',
                'Identifiant de la collectivité à la source',
                'Création',
                'Dernière mise à jour',
            ],
            [
                '',
                'Plateforme X',
                'Saint-Didier',
                'https://saint-didier.fr/',
                'saint-didier',
                '3 avril 2023 12:14',
                '3 avril 2023 12:14',
            ],
        ]

    def test_lieux(self, app):
        resp = app.get('/').maybe_follow()
        resp = resp.click('Lieux')
        assert table_text(resp.pyquery('#result_list')) == [
            ['', 'Collectivité', 'Nom', 'Création', 'Dernière mise à jour'],
            ['', 'Saint-Didier', 'Mairie', '3 avril 2023 12:15', '3 avril 2023 12:15'],
        ]

    def test_plages(self, app):
        resp = app.get('/').maybe_follow()
        resp = resp.click('Lieux')
        assert table_text(resp.pyquery('#result_list')) == [
            ['', 'Collectivité', 'Nom', 'Création', 'Dernière mise à jour'],
            ['', 'Saint-Didier', 'Mairie', '3 avril 2023 12:15', '3 avril 2023 12:15'],
        ]

    def test_rdvs(self, app):
        resp = app.get('/').maybe_follow()
        resp = resp.click('Rendez-vous')
        assert table_text(resp.pyquery('#result_list')) == [
            ['', '1\nDate', 'Lieu', '2\nIdentifiant de prédemande', 'Création', 'Dernière mise à jour'],
            [
                '',
                '3 avril 2023 12:15',
                'Mairie / 2 rue du four / Saint-Didier',
                'ABCD123456',
                '3 avril 2023 12:16',
                '3 avril 2023 12:16',
            ],
        ]
