# ANTS-Hub - Copyright (C) Entr'ouvert

import freezegun
import pytest


@pytest.fixture
def freezer():
    with freezegun.freeze_time() as freezer:
        yield freezer
