# ANTS-Hub - Copyright (C) Entr'ouvert

import copy
import datetime
import urllib.parse

import pytest
import responses
import responses.matchers
from django.core.management import call_command

from ants_hub.api.ants import ANTSError, get_status_of_predemandes
from ants_hub.data.models import Config, Lieu, Plage, RendezVous, TypeDeRdv
from ants_hub.timezone import now


@pytest.mark.parametrize(
    'endpoint', ['getManagedMeetingPoints', 'availableTimeSlots', 'searchApplicationIds']
)
def test_authentication(endpoint, django_app, db):
    django_app.get(f'/api/ants/{endpoint}', status=401)

    django_app.get(f'/api/ants/{endpoint}', headers={'X-Hub-Rdv-Auth-Token': 'abcd'}, status=401)
    Config.set(Config.REQUEST_FROM_ANTS_AUTH_TOKEN, 'abcd')
    django_app.get(f'/api/ants/{endpoint}', headers={'X-Hub-Rdv-Auth-Token': 'abcd'}, status=(200, 422))


class TestEndpoints:
    @pytest.fixture(autouse=True)
    def setup(self, db, settings, django_app, freezer):
        Config.set(Config.REQUEST_FROM_ANTS_AUTH_TOKEN, 'abcd')
        django_app.extra_environ['HTTP_X_HUB_RDV_AUTH_TOKEN'] = 'abcd'
        call_command('loaddata', 'fixtures/example1.json')
        freezer.move_to('2023-04-03T12:00:00+02:00')

    @pytest.fixture
    def lieu(self):
        return Lieu.objects.get()

    def test_managed_meeting_points(self, db, django_app):
        lieu = Lieu.objects.get()

        # Add a lieu with no plage, to check it is not returned
        lieu_with_no_plage = copy.copy(lieu)
        lieu_with_no_plage.id = None
        lieu_with_no_plage.source_id = '1234'
        lieu_with_no_plage.nom = 'Annexe'
        lieu_with_no_plage.save()
        assert Lieu.objects.count() == 2

        assert lieu.last_gmmp is None
        response = django_app.get('/api/ants/getManagedMeetingPoints')
        assert response.json == [
            {
                'city_name': 'Saint-Didier',
                'id': '1',
                'latitude': 2.0,
                'longitude': 1.0,
                'name': 'Mairie',
                'public_entry_address': '2 rue  du four',
                'zip_code': '99999',
                'website': 'https://saint-didier.fr/',
                'city_logo': 'https://saint-didier.fr/logo.png',
            },
        ]
        lieu.refresh_from_db()
        assert lieu.last_gmmp is not None
        assert (now() - lieu.last_gmmp).total_seconds() < 600

        # Add a plage to lieu_with_no_plage
        Plage.objects.create(
            lieu=lieu_with_no_plage,
            date=datetime.date.today(),
            horaires='10:00+02:00-12:00+02:00,14:00+02:00-17:00+02:00',
            duree=30,
            type_de_rdv=TypeDeRdv.CNI,
            personnes=1,
        )
        assert len(django_app.get('/api/ants/getManagedMeetingPoints').json) == 2

    def test_available_time_slots(self, db, django_app, lieu):
        foobar = Lieu.objects.create(
            collectivite=lieu.collectivite,
            nom='FooBar',
            source_id='1234',
            numero_rue='1',
            code_postal='99999',
            ville='FooBar',
            longitude=4.32,
            latitude=10.4,
        )

        for i in range(5, 10):
            # open new RdV from 8th april to 12th april
            Plage.objects.create(
                lieu=lieu,
                date=datetime.date.today() + datetime.timedelta(days=i),
                horaires='10:00+02:00-12:00+02:00,14:00+02:00-17:00+02:00',
                duree=30,
                type_de_rdv=TypeDeRdv.CNI,
                personnes=1,
            )
            Plage.objects.create(
                lieu=foobar,
                date=datetime.date.today() + datetime.timedelta(days=i),
                horaires='10:00+02:00-12:00+02:00,14:00+02:00-17:00+02:00',
                duree=30,
                type_de_rdv=TypeDeRdv.CNI,
                personnes=1,
            )
        response = django_app.get(
            '/api/ants/availableTimeSlots',
            params={
                'meeting_point_ids': str(lieu.id),
                'start_date': '2023-04-09',
                'end_date': '2023-04-11',
            },
        )
        assert set(response.json.keys()) == {str(lieu.id)}
        slots = response.json[str(lieu.id)]
        # 4 meetings possible in the morning, 6 in the evening, for
        # 3 days = 30 slots
        assert len(slots) == 30
        for slot in slots:
            assert 9 <= datetime.datetime.fromisoformat(slot['datetime'].rstrip('Z')).day <= 11
            # remove the Z suffix
            assert slot['datetime'][:-1] in slot['callback_url']
            assert f'{lieu.slug}-{lieu.id}' in slot['callback_url']
            assert f'{lieu.collectivite.slug}-{lieu.collectivite.id}' in slot['callback_url']
        response = django_app.get(slots[-1]['callback_url'])
        assert response.location == 'https://saint-didier.fr/rdv/'
        lieu.rdv_url = 'https://saint-didier.fr/mairie/rdv/'
        lieu.save()
        response = django_app.get(slots[-1]['callback_url'])
        assert response.location == 'https://saint-didier.fr/mairie/rdv/'

    def test_available_time_slots_reason(self, db, django_app, lieu):
        Plage.objects.all().delete()
        kwargs = {
            'lieu': lieu,
            'date': datetime.date.today() + datetime.timedelta(days=1),
            'duree': 30,
        }
        Plage.objects.create(**kwargs, type_de_rdv=TypeDeRdv.CNI, horaires='10:00+02:00-10:30+02:00')
        Plage.objects.create(**kwargs, type_de_rdv=TypeDeRdv.PASSPORT, horaires='10:30+02:00-11:00+02:00')
        Plage.objects.create(**kwargs, type_de_rdv=TypeDeRdv.CNI_PASSPORT, horaires='11:00+02:00-11:30+02:00')

        # default is CNI
        response = django_app.get(
            '/api/ants/availableTimeSlots',
            params={
                'meeting_point_ids': str(lieu.id),
                'start_date': '2023-04-03',
                'end_date': '2023-04-05',
            },
        )
        assert len(response.json[str(lieu.id)]) == 1
        assert {x['datetime'] for x in response.json[str(lieu.id)]} == {'2023-04-04T10:00:00Z'}

        # CNI matches CNI and CNI-PASSPORT
        response = django_app.get(
            '/api/ants/availableTimeSlots',
            params={
                'meeting_point_ids': str(lieu.id),
                'start_date': '2023-04-03',
                'end_date': '2023-04-05',
                'reason': 'CNI',
            },
        )
        assert len(response.json) == 1
        assert len(response.json[str(lieu.id)]) == 1
        assert {x['datetime'] for x in response.json[str(lieu.id)]} == {'2023-04-04T10:00:00Z'}

        response = django_app.get(
            '/api/ants/availableTimeSlots',
            params={
                'meeting_point_ids': str(lieu.id),
                'start_date': '2023-04-03',
                'end_date': '2023-04-05',
                'reason': 'PASSPORT',
            },
        )
        assert len(response.json) == 1
        assert len(response.json[str(lieu.id)]) == 1
        assert {x['datetime'] for x in response.json[str(lieu.id)]} == {'2023-04-04T10:30:00Z'}

        response = django_app.get(
            '/api/ants/availableTimeSlots',
            params={
                'meeting_point_ids': str(lieu.id),
                'start_date': '2023-04-03',
                'end_date': '2023-04-05',
                'reason': 'CNI-PASSPORT',
            },
        )
        assert len(response.json) == 1
        assert len(response.json[str(lieu.id)]) == 1
        assert {x['datetime'] for x in response.json[str(lieu.id)]} == {'2023-04-04T11:00:00Z'}

    def test_available_time_slots_documents_number(self, db, django_app, lieu):
        Plage.objects.all().delete()
        kwargs = {
            'lieu': lieu,
            'date': datetime.date.today() + datetime.timedelta(days=1),
            'duree': 30,
            'type_de_rdv': TypeDeRdv.CNI,
        }
        Plage.objects.create(**kwargs, personnes=1, horaires='10:00+02:00-10:30+02:00')
        Plage.objects.create(**kwargs, personnes=2, horaires='10:30+02:00-11:00+02:00')
        Plage.objects.create(**kwargs, personnes=3, horaires='11:00+02:00-11:30+02:00')
        Plage.objects.create(**kwargs, personnes=4, horaires='12:00+02:00-12:30+02:00')
        Plage.objects.create(**kwargs, personnes=5, horaires='12:30+02:00-13:00+02:00')

        # default is 1 person
        response = django_app.get(
            '/api/ants/availableTimeSlots',
            params={
                'meeting_point_ids': str(lieu.id),
                'start_date': '2023-04-03',
                'end_date': '2023-04-05',
            },
        )
        assert len(response.json) == 1
        assert len(response.json[str(lieu.id)]) == 1
        assert {x['datetime'] for x in response.json[str(lieu.id)]} == {'2023-04-04T10:00:00Z'}

        response = django_app.get(
            '/api/ants/availableTimeSlots',
            params={
                'meeting_point_ids': str(lieu.id),
                'start_date': '2023-04-03',
                'end_date': '2023-04-05',
                'documents_number': 1,
            },
        )
        assert len(response.json) == 1
        assert len(response.json[str(lieu.id)]) == 1
        assert {x['datetime'] for x in response.json[str(lieu.id)]} == {'2023-04-04T10:00:00Z'}

        response = django_app.get(
            '/api/ants/availableTimeSlots',
            params={
                'meeting_point_ids': str(lieu.id),
                'start_date': '2023-04-03',
                'end_date': '2023-04-05',
                'documents_number': 2,
            },
        )
        assert len(response.json) == 1
        assert len(response.json[str(lieu.id)]) == 1
        assert {x['datetime'] for x in response.json[str(lieu.id)]} == {'2023-04-04T10:30:00Z'}

        response = django_app.get(
            '/api/ants/availableTimeSlots',
            params={
                'meeting_point_ids': str(lieu.id),
                'start_date': '2023-04-03',
                'end_date': '2023-04-05',
                'documents_number': 3,
            },
        )
        assert len(response.json) == 1
        assert len(response.json[str(lieu.id)]) == 1
        assert {x['datetime'] for x in response.json[str(lieu.id)]} == {'2023-04-04T11:00:00Z'}

        # implicit maximum is 5
        response = django_app.get(
            '/api/ants/availableTimeSlots',
            params={
                'meeting_point_ids': str(lieu.id),
                'start_date': '2023-04-03',
                'end_date': '2023-04-05',
                'documents_number': 6,
            },
        )
        assert len(response.json) == 1
        assert len(response.json[str(lieu.id)]) == 1
        assert {x['datetime'] for x in response.json[str(lieu.id)]} == {'2023-04-04T12:30:00Z'}

    def test_search_application_ids(self, db, django_app, lieu):
        RendezVous.objects.create(
            uuid='7621a90a-2dd3-44e5-9df7-879abddeaad5',
            lieu=lieu,
            identifiant_predemande='123456',
            date=datetime.datetime.fromisoformat('2023-04-11T11:00:00+02:00'),
            gestion_url='https://monrdv.fr/gestion/',
            annulation_url='https://monrdv.fr/annulation/',
        )
        RendezVous.objects.create(
            uuid='7621a90a-2dd3-44e5-9df7-879abddeaad6',
            lieu=lieu,
            identifiant_predemande='123456',
            date=datetime.datetime.fromisoformat('2023-04-11T11:00:00+02:00'),
            canceled=datetime.datetime.fromisoformat('2023-04-12T11:00:00+02:00'),
            gestion_url='https://monrdv.fr/gestion/',
            annulation_url='https://monrdv.fr/annulation/',
        )
        response = django_app.get(
            '/api/ants/searchApplicationIds',
            params={
                'application_ids': ['abCD123456', '123456'],
            },
        )
        assert response.json == {
            '123456': [
                {
                    'cancel_url': 'https://ants-hub.entrouvert.org/rdv/saint-didier-1/mairie-1/2023-04-11T11:00:00+02:00/'
                    'annulation/7621a90a-2dd3-44e5-9df7-879abddeaad5/',
                    'datetime': '2023-04-11T11:00:00Z',
                    'management_url': 'https://ants-hub.entrouvert.org/rdv/saint-didier-1/mairie-1/2023-04-11T11:00:00+02:00/'
                    'gestion/7621a90a-2dd3-44e5-9df7-879abddeaad5/',
                    'meeting_point': 'Mairie',
                }
            ],
            'ABCD123456': [
                {
                    'cancel_url': 'https://ants-hub.entrouvert.org/rdv/saint-didier-1/mairie-1/2023-04-03T12:15:00+02:00/'
                    'annulation/7cace277-9157-4fbc-9705-45522984805d/',
                    'datetime': '2023-04-03T12:15:00Z',
                    'management_url': 'https://ants-hub.entrouvert.org/rdv/saint-didier-1/mairie-1/2023-04-03T12:15:00+02:00/'
                    'gestion/7cace277-9157-4fbc-9705-45522984805d/',
                    'meeting_point': 'Mairie',
                }
            ],
        }
        locations = {
            identifiant_predemande: [
                {k: django_app.get(v).location for k, v in rdv.items() if k.endswith('_url')}
                for rdv in response.json[identifiant_predemande]
            ]
            for identifiant_predemande in response.json
        }
        assert locations == {
            '123456': [
                {
                    'cancel_url': 'https://monrdv.fr/annulation/',
                    'management_url': 'https://monrdv.fr/gestion/',
                }
            ],
            'ABCD123456': [
                {
                    'cancel_url': 'https://saint-didier.fr/mairie/annulation/',
                    'management_url': 'https://saint-didier/gestion/',
                }
            ],
        }

    class TestParameterError:
        def test_available_time_slots(self, db, django_app):
            django_app.get('/api/ants/availableTimeSlots', params={}, status=422)
            django_app.get('/api/ants/availableTimeSlots', params={'meeting_point_ids': 'xyz'}, status=422)
            django_app.get('/api/ants/availableTimeSlots', params={'meeting_point_ids': '1'}, status=422)
            django_app.get(
                '/api/ants/availableTimeSlots',
                params={'meeting_point_ids': '1', 'start_date': 'abcd'},
                status=422,
            )
            django_app.get(
                '/api/ants/availableTimeSlots',
                params={'meeting_point_ids': '1', 'start_date': '2023-03-05'},
                status=422,
            )
            django_app.get(
                '/api/ants/availableTimeSlots',
                params={'meeting_point_ids': '1', 'start_date': '2023-04-05'},
                status=422,
            )
            django_app.get(
                '/api/ants/availableTimeSlots',
                params={'meeting_point_ids': '1', 'start_date': '2023-04-05', 'end_date': 'abcd'},
                status=200,
            )
            django_app.get(
                '/api/ants/availableTimeSlots',
                params={'meeting_point_ids': '1', 'start_date': 'abcd', 'end_date': 'abcd'},
                status=200,
            )
            django_app.get(
                '/api/ants/availableTimeSlots',
                params={'meeting_point_ids': '1', 'start_date': '2023-04-05', 'end_date': '2023-02-01'},
                status=200,
            )
            django_app.get(
                '/api/ants/availableTimeSlots',
                params={
                    'meeting_point_ids': '1',
                    'start_date': '2023-04-05',
                    'end_date': '2023-04-10',
                    'reason': 'xyz',
                },
                status=200,
            )
            django_app.get(
                '/api/ants/availableTimeSlots',
                params={
                    'meeting_point_ids': '1',
                    'start_date': '2023-04-05',
                    'end_date': '2023-04-10',
                    'reason': 'xyz',
                    'documents_number': 'zbc',
                },
                status=200,
            )
            django_app.get(
                '/api/ants/availableTimeSlots',
                params={
                    'meeting_point_ids': '1',
                    'start_date': '2023-04-05',
                    'end_date': '2023-04-10',
                    'reason': 'xyz',
                    'documents_number': '10',
                },
                status=200,
            )

        def test_search_application_ids(self, db, django_app):
            django_app.get('/api/ants/searchApplicationIds', params={}, status=422)


class TestAPIV2Push:
    @pytest.fixture(autouse=True)
    def setup(self, db, settings, django_app, freezer):
        Config.set(Config.REQUEST_TO_ANTS_V2_AUTH_TOKEN, 'abcd')
        freezer.move_to('2023-04-03T12:20:00+02:00')
        call_command('loaddata', 'fixtures/example1.json')

    @responses.activate
    def test_push(self, db, freezer):
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(canceled__isnull=True).count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json={
                'ABCD123456': {
                    'appointments': [
                        {
                            'meeting_point': 'Mairie de pouetpouet',
                            'appointment_date': '2023-04-03T12:15:00',
                        }
                    ]
                }
            },
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )

        post_response = responses.add(
            responses.POST,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            json={'success': True},
            status=200,
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
        )
        delete_response = responses.add(
            responses.DELETE,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            json={'rowcount': 1},
            status=200,
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
        )

        call_command('upload-rdvs')

        assert post_response.call_count == 1
        assert delete_response.call_count == 0

        called_url = urllib.parse.urlparse(responses._default_mock.calls[-1].request.url)
        called_qs = urllib.parse.parse_qsl(called_url.query, keep_blank_values=True)
        assert called_url._replace(query='')._asdict() == {
            'scheme': 'https',
            'netloc': 'api-coordination.rendezvouspasseport.ants.gouv.fr',
            'path': '/api/appointments',
            'params': '',
            'query': '',
            'fragment': '',
        }
        assert called_qs == [
            ('application_id', 'ABCD123456'),
            (
                'management_url',
                'https://ants-hub.entrouvert.org/rdv/saint-didier-1/mairie-1/2023-04-03T12:15:00+02:00/gestion/7cace277-9157-4fbc-9705-45522984805d/',
            ),
            ('meeting_point', 'Mairie'),
            ('meeting_point_id', '1'),
            ('appointment_date', '2023-04-03 12:15:00'),
        ]

        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 0

        call_command('upload-rdvs')

        assert post_response.call_count == 1
        assert delete_response.call_count == 0

        freezer.move_to('2023-04-03T13:00:00+02:00')

        RendezVous.objects.update(canceled=now(), last_update=now())

        responses.replace(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json={
                'ABCD123456': {
                    'appointments': [
                        {
                            'meeting_point': 'Mairie',
                            'appointment_date': '2023-04-03T12:15:00',
                        }
                    ]
                }
            },
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )

        call_command('upload-rdvs')

        assert post_response.call_count == 1
        assert delete_response.call_count == 1

        called_url = urllib.parse.urlparse(responses._default_mock.calls[-1].request.url)
        called_qs = urllib.parse.parse_qsl(called_url.query, keep_blank_values=True)
        assert called_url._replace(query='')._asdict() == {
            'scheme': 'https',
            'netloc': 'api-coordination.rendezvouspasseport.ants.gouv.fr',
            'path': '/api/appointments',
            'params': '',
            'query': '',
            'fragment': '',
        }
        assert called_qs == [
            ('application_id', 'ABCD123456'),
            ('meeting_point', 'Mairie'),
            ('meeting_point_id', '1'),
            ('appointment_date', '2023-04-03 12:15:00'),
        ]

    @responses.activate
    @pytest.mark.parametrize(
        'response_kwargs',
        [
            {
                'json': {},
                'status': 200,
            },
            {
                'json': None,
                'status': 200,
            },
            {
                'json': '',
                'status': 200,
            },
            {
                'json': {'detail': []},
                'status': 422,
            },
            {
                'status': 500,
            },
        ],
        ids=[
            'JSON is empty dict',
            'JSON is null',
            'JSON is empty string',
            'HTTP 422 with details',
            'HTTP 500',
        ],
    )
    def test_post_error(self, response_kwargs, db, freezer):
        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json={},
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )
        post_response = responses.add(
            responses.POST,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
            **response_kwargs,
        )
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

        call_command('upload-rdvs')

        assert post_response.call_count == 1
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

    @responses.activate
    @pytest.mark.parametrize(
        'response_kwargs',
        [
            {
                'json': {},
                'status': 200,
            },
            {
                'json': None,
                'status': 200,
            },
            {
                'json': '',
                'status': 200,
            },
            {
                'json': {'detail': []},
                'status': 422,
            },
            {
                'status': 500,
            },
        ],
        ids=[
            'JSON is empty dict',
            'JSON is null',
            'JSON is empty string',
            'HTTP 422 with details',
            'HTTP 500',
        ],
    )
    def test_delete_error(self, response_kwargs, db, freezer):
        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json={
                'ABCD123456': {
                    'appointments': [
                        {
                            'meeting_point': 'Mairie',
                            'appointment_date': '2023-04-03T12:15:00',
                        }
                    ]
                }
            },
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )
        post_response = responses.add(
            responses.DELETE,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
            **response_kwargs,
        )
        RendezVous.objects.update(canceled=now(), last_update=now())
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

        call_command('upload-rdvs')

        assert post_response.call_count == 1
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

    @responses.activate
    def test_delete_duplicates(self, db, freezer):
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(canceled__isnull=True).count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json={
                'ABCD123456': {
                    'appointments': [
                        {
                            'meeting_point': 'Mairie',
                            'appointment_date': '2023-04-03T12:15:00',
                        },
                        {
                            'meeting_point': 'Mairie',
                            'appointment_date': '2023-04-03T12:15:00',
                        },
                    ]
                }
            },
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )

        post_response = responses.add(
            responses.POST,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            json={'success': True},
            status=200,
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
        )
        delete_response = responses.add(
            responses.DELETE,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            json={'rowcount': 1},
            status=200,
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
        )

        call_command('upload-rdvs')

        assert post_response.call_count == 1
        assert delete_response.call_count == 1

    @responses.activate
    @pytest.mark.parametrize(
        'response_kwargs',
        [
            {
                'json': {},
                'status': 200,
            },
            {
                'json': None,
                'status': 200,
            },
            {
                'json': '',
                'status': 200,
            },
            {
                'json': {'detail': []},
                'status': 422,
            },
            {
                'status': 500,
            },
        ],
        ids=[
            'JSON is empty dict',
            'JSON is null',
            'JSON is empty string',
            'HTTP 422 with details',
            'HTTP 500',
        ],
    )
    def test_delete_duplicates_error(self, response_kwargs, db, freezer):
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(canceled__isnull=True).count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json={
                'ABCD123456': {
                    'appointments': [
                        {
                            'meeting_point': 'Mairie',
                            'appointment_date': '2023-04-03T12:15:00',
                        },
                        {
                            'meeting_point': 'Mairie',
                            'appointment_date': '2023-04-03T12:15:00',
                        },
                    ]
                }
            },
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )

        post_response = responses.add(
            responses.POST,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            json={'success': True},
            status=200,
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
        )
        delete_response = responses.add(
            responses.DELETE,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
            **response_kwargs,
        )

        RendezVous.objects.update(canceled=now(), last_update=now())
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

        call_command('upload-rdvs')

        assert post_response.call_count == 0
        assert delete_response.call_count == 1
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

    @responses.activate
    def test_delete_management_url_changed(self, db, freezer):
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(canceled__isnull=True).count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json={
                'ABCD123456': {
                    'appointments': [
                        {
                            'meeting_point': 'Mairie',
                            'appointment_date': '2023-04-03T12:15:00',
                            'management_url': 'xyz',
                        },
                    ]
                }
            },
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )

        post_response = responses.add(
            responses.POST,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            json={'success': True},
            status=200,
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
        )
        delete_response = responses.add(
            responses.DELETE,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            json={'rowcount': 1},
            status=200,
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
        )

        call_command('upload-rdvs')

        assert post_response.call_count == 1
        assert delete_response.call_count == 1

    @responses.activate
    def test_create_already_exists(self, db, freezer):
        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json={
                'ABCD123456': {
                    'status': 'validated',
                    'appointments': [
                        {
                            'meeting_point': 'Mairie',
                            'appointment_date': '2023-04-03T12:15:00',
                            'management_url': (
                                'https://ants-hub.entrouvert.org/rdv/saint-didier-1/'
                                'mairie-1/2023-04-03T12:15:00+02:00/gestion/7cace277-9157-4fbc-9705-45522984805d/'
                            ),
                        }
                    ],
                }
            },
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )
        post_response = responses.add(
            responses.POST,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            json={'success': True},
            status=200,
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
        )
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

        call_command('upload-rdvs')

        assert post_response.call_count == 0
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=False).count() == 1

    @responses.activate
    def test_delete_already_deleted(self, db, freezer):
        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json={},
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )
        delete_response = responses.add(
            responses.DELETE,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            json={'rowcount': 1},
            status=200,
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
        )
        RendezVous.objects.update(canceled=now(), last_update=now())
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

        call_command('upload-rdvs')

        assert delete_response.call_count == 0
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=False).count() == 1

    @responses.activate
    def test_identifiant_predemande_invalide(self, db, freezer, caplog):
        caplog.set_level('INFO')

        RendezVous.objects.update(identifiant_predemande='NONE')

        call_command('upload-rdvs')

        # check a message is logged
        assert caplog.messages == [
            'ignored because identifiant_predemande is malformed rdv(2023-04-03 10:15:00+00:00 / NONE) of lieu Mairie / 2 rue  du four / Saint-Didier'
        ]

    @responses.activate
    def test_create_application_id_is_unknown(self, db, freezer, caplog):
        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json={
                'ABCD123456': {
                    'status': 'unknown',
                    'appointments': [],
                }
            },
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )

        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

        caplog.set_level('INFO')
        call_command('upload-rdvs')

        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=False).count() == 1

        # check a message is logged
        assert caplog.messages == [
            'ignored because identifiant_predemande is unknown rdv(2023-04-03 10:15:00+00:00 / ABCD123456) of lieu Mairie / 2 rue  du four / Saint-Didier',
        ]

    @responses.activate
    def test_create_application_id_not_found(self, db, freezer, caplog):
        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json={
                'ABCD123456': {
                    'status': 'validated',
                    'appointments': [],
                }
            },
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )
        post_response = responses.add(
            responses.POST,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/appointments',
            json={'detail': 'Application not found in database'},
            status=404,
            match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
        )
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=True).count() == 1

        caplog.set_level('INFO')
        call_command('upload-rdvs')

        assert post_response.call_count == 1
        assert RendezVous.objects.count() == 1
        assert RendezVous.objects.filter(last_upload__isnull=False).count() == 1

        # check a message is logged
        assert caplog.messages == [
            'ignored because identifiant_predemande does not exist rdv(2023-04-03 10:15:00+00:00 / ABCD123456) of lieu Mairie / 2 rue  du four / Saint-Didier',
        ]


class TestGetStatusOfPredemandes:
    @pytest.fixture(autouse=True)
    def setup(self, db, settings, django_app, freezer):
        Config.set(Config.REQUEST_TO_ANTS_V2_AUTH_TOKEN, 'abcd')

    @responses.activate
    def test_valid(self, db):
        document = {
            'ABCDE12345': {
                'status': 'validated',
                'appointments': [],
            },
            '1234567890': {
                'status': 'validated',
                'appointments': [],
            },
        }
        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json=document,
            status=200,
            match=[
                responses.matchers.query_string_matcher(
                    'application_ids=ABCDE12345&application_ids=1234567890&meeting_point_id=1234'
                ),
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )
        valid, msg, data, appointments = get_status_of_predemandes(
            ['ABCDE12345', '1234567890'], meeting_point_id='1234'
        )

        assert valid
        assert msg == ''
        assert data == document
        assert not appointments

    @responses.activate
    @pytest.mark.parametrize(
        'document,expected_message,expected_appointments',
        [
            (
                {
                    'ABCDE12345': {
                        'status': 'unknown',
                        'appointments': [],
                    },
                    '1234567890': {
                        'status': 'validated',
                        'appointments': [],
                    },
                },
                'Prédemande "ABCDE12345" inconnue, expirée ou déjà consommée.',
                [],
            ),
            (
                {
                    '1234567890': {
                        'status': 'validated',
                        'appointments': [],
                    },
                },
                'Prédemande "ABCDE12345" inconnue.',
                [],
            ),
            (
                {
                    'ABCDE12345': {
                        'status': 'validated',
                        'appointments': [],
                    },
                    '1234567890': {
                        'status': 'validated',
                        'appointments': [
                            {
                                'management_url': 'https://rdvenmairie.fr/gestion/login?ants=83AHERZY8F&appointment_id=64594c435d7bfc0012fa8c87&canceled=true',
                                'meeting_point': 'Mairie de Luisant',
                                'appointment_date': '2023-09-20T09:00:11',
                            }
                        ],
                    },
                },
                'Prédemande "1234567890" déjà liée à un ou des rendez-vous.',
                [
                    {
                        'identifiant_predemande': '1234567890',
                        'management_url': 'https://rdvenmairie.fr/gestion/login?ants=83AHERZY8F&appointment_id=64594c435d7bfc0012fa8c87&canceled=true',
                        'meeting_point': 'Mairie de Luisant',
                        'appointment_date': '2023-09-20T09:00:11',
                    }
                ],
            ),
        ],
        ids=[
            'one status is unknown',
            'missing application_id',
            'appointments exists',
        ],
    )
    def test_invalid(self, document, expected_message, expected_appointments, db):
        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json=document,
            status=200,
            match=[
                responses.matchers.query_string_matcher(
                    'application_ids=ABCDE12345&application_ids=1234567890'
                ),
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )
        valid, msg, data, appointments = get_status_of_predemandes(['ABCDE12345', '1234567890'])

        assert not valid
        assert msg == expected_message
        assert data == document
        assert appointments == expected_appointments

    @responses.activate
    @pytest.mark.parametrize(
        'response_kwargs',
        [
            {
                'json': None,
                'status': 200,
            },
            {
                'json': '',
                'status': 200,
            },
            {
                'status': 500,
            },
        ],
        ids=[
            'JSON is null',
            'JSON is string',
            'HTTP 500',
        ],
    )
    def test_error(self, response_kwargs, db):
        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            match=[
                responses.matchers.query_string_matcher(
                    'application_ids=ABCDE12345&application_ids=1234567890'
                ),
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
            **response_kwargs,
        )
        with pytest.raises(ANTSError):
            get_status_of_predemandes(['ABCDE12345', '1234567890'])
