# ANTS-Hub - Copyright (C) Entr'ouvert

import datetime
import zoneinfo

import pytest
import requests
import responses
import responses.matchers
from django.core.management import call_command
from django.db import OperationalError
from django.utils.timezone import now

from ants_hub.api.views.chrono import RendezVousDisponibleView
from ants_hub.data.models import Collectivite, Config, Lieu, Plage, Raccordement, RendezVous


@pytest.fixture(autouse=True)
def setup(freezer):
    freezer.move_to('2023-03-03T12:00:00+02:00')


def test_ping(django_app, db):
    django_app.get('/api/chrono/ping/', status=401)
    django_app.get('/api/chrono/ping/', headers={'Authorization': ''}, status=401)
    django_app.get('/api/chrono/ping/', headers={'Authorization': 'Basic abc'}, status=401)
    django_app.set_authorization(('Basic', ('abcd', '')))
    django_app.get('/api/chrono/ping/', status=401)
    Raccordement.objects.create(name='plateforme', apikey='abcd')
    assert django_app.get('/api/chrono/ping/').json == {'err': 0}


def test_rendez_vous_disponibles(django_app, db, caplog):
    Raccordement.objects.create(name='plateforme', apikey='abcd')
    django_app.set_authorization(('Basic', ('abcd', '')))
    assert django_app.get('/api/chrono/rendez-vous-disponibles/').json == {
        'err': 0,
        'collectivites': [],
    }
    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'nom': 'Saint-Didier',
                    'url': 'https://saint-didier.fr/rdv/',
                    'logo_url': 'https://saint-didier.fr/logo.png',
                }
            ]
        },
    )
    assert response.json == {
        'err': 0,
        'data': {
            'collectivites_created': 1,
            'collectivites_updated': 0,
            'lieux_created': 0,
            'lieux_updated': 0,
            'lieux_deleted': 0,
            'plage_created': 0,
            'plage_updated': 0,
            'plage_deleted': 0,
            'rdv_created': 0,
            'rdv_updated': 0,
            'rdv_deleted': 0,
        },
    }

    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'nom': 'Saint-Didier',
                    'url': 'https://saint-didier.fr/rdv2/',
                    'logo_url': 'https://saint-didier.fr/logo.png',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'nom': 'Mairie de Saint-Didier',
                            'numero_rue': '2 rue du four',
                            'code_postal': '99999',
                            'ville': 'Saint-Didier',
                            'longitude': 1.5,
                            'latitude': 2.3,
                            'logo_url': 'https://saint-didier.fr/logo-mairie.png',
                            'url': 'https://saint-didier.fr/mairie/',
                            'rdv_url': 'https://saint-didier.fr/mairie/rdv/',
                            'gestion_url': 'https://saint-didier.fr/mairie/gestion/',
                            'annulation_url': 'https://saint-didier.fr/mairie/annulation/',
                        }
                    ],
                }
            ]
        },
    )
    assert response.json == {
        'err': 0,
        'data': {
            'collectivites_created': 0,
            'collectivites_updated': 1,
            'lieux_created': 1,
            'lieux_updated': 0,
            'lieux_deleted': 0,
            'plage_created': 0,
            'plage_updated': 0,
            'plage_deleted': 0,
            'rdv_created': 0,
            'rdv_updated': 0,
            'rdv_deleted': 0,
        },
    }

    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                }
                            ],
                        }
                    ],
                }
            ]
        },
    )
    assert response.json == {
        'err': 0,
        'data': {
            'collectivites_created': 0,
            'collectivites_updated': 0,
            'lieux_created': 0,
            'lieux_updated': 0,
            'lieux_deleted': 0,
            'plage_created': 2,
            'plage_updated': 0,
            'plage_deleted': 0,
            'rdv_created': 0,
            'rdv_updated': 0,
            'rdv_deleted': 0,
        },
    }

    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['PASSPORT'],
                                }
                            ],
                        }
                    ],
                }
            ]
        },
    )
    assert response.json == {
        'err': 0,
        'data': {
            'collectivites_created': 0,
            'collectivites_updated': 0,
            'lieux_created': 0,
            'lieux_updated': 0,
            'lieux_deleted': 0,
            'plage_created': 0,
            'plage_updated': 0,
            'plage_deleted': 1,
            'rdv_created': 0,
            'rdv_updated': 0,
            'rdv_deleted': 0,
        },
    }

    caplog.clear()
    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'rdvs': [
                                {
                                    'id': '123456ABCD',
                                    'date': '2023-03-20T15:00:00+02:00',
                                },
                                {
                                    'id': 'None',
                                    'date': '2023-03-20T15:00:00+02:00',
                                },
                            ],
                        }
                    ],
                }
            ]
        },
    )
    assert response.json == {
        'err': 0,
        'data': {
            'collectivites_created': 0,
            'collectivites_updated': 0,
            'lieux_created': 0,
            'lieux_updated': 0,
            'lieux_deleted': 0,
            'plage_created': 0,
            'plage_updated': 0,
            'plage_deleted': 0,
            'rdv_created': 1,
            'rdv_updated': 0,
            'rdv_deleted': 0,
        },
    }

    assert caplog.messages == [
        'rendez-vous-disponibles(plateforme) identifiant_predemande invalid "NONE" '
        'on lieu "Mairie de Saint-Didier / 2 rue du four / Saint-Didier"',
        'rendez-vous-disponibles(plateforme) created 1 rdv',
    ]

    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'rdvs': [
                                {
                                    'id': '123456ABCD',
                                    'date': '2023-03-20T15:00:00+02:00',
                                    'annule': True,
                                }
                            ],
                        }
                    ],
                }
            ]
        },
    )
    assert response.json == {
        'err': 0,
        'data': {
            'collectivites_created': 0,
            'collectivites_updated': 0,
            'lieux_created': 0,
            'lieux_updated': 0,
            'lieux_deleted': 0,
            'plage_created': 0,
            'plage_updated': 0,
            'plage_deleted': 0,
            'rdv_created': 0,
            'rdv_updated': 0,
            'rdv_deleted': 1,
        },
    }

    response = django_app.post('/api/chrono/rendez-vous-disponibles/', params=b'xxx', status=400)
    assert response.json['err'] == 'invalid-json'

    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'rdvs': [
                                {
                                    'id': 'abcd',
                                }
                            ],
                        }
                    ],
                }
            ]
        },
        status=400,
    )
    assert response.json['err'] == 'invalid-json'

    assert django_app.get('/api/chrono/rendez-vous-disponibles/').json == {
        'err': 0,
        'collectivites': [
            {
                'id': 'col1',
                'nom': 'Saint-Didier',
                'url': 'https://saint-didier.fr/rdv2/',
                'logo_url': 'https://saint-didier.fr/logo.png',
                'created': '2023-03-03T10:00:00+00:00',
                'last_update': '2023-03-03T10:00:00+00:00',
                'lieux': [
                    {
                        'id': 'lieu1',
                        'latitude': 2.3,
                        'longitude': 1.5,
                        'nom': 'Mairie de Saint-Didier',
                        'numero_rue': '2 rue du four',
                        'code_postal': '99999',
                        'ville': 'Saint-Didier',
                        'nombre_de_jours_avec_rdv_disponibles': 1,
                        'nombre_de_pre_demandes_actives': 0,
                        'logo_url': 'https://saint-didier.fr/logo-mairie.png',
                        'url': 'https://saint-didier.fr/mairie/',
                        'rdv_url': 'https://saint-didier.fr/mairie/rdv/',
                        'gestion_url': 'https://saint-didier.fr/mairie/gestion/',
                        'annulation_url': 'https://saint-didier.fr/mairie/annulation/',
                        'last_gmmp': None,
                    }
                ],
                'nombre_de_jours_avec_rdv_disponibles': 1,
                'nombre_de_lieux': 1,
                'nombre_de_pre_demandes_actives': 0,
            }
        ],
    }

    last_gmmp = now()
    Lieu.objects.update(last_gmmp=last_gmmp)

    assert django_app.get('/api/chrono/rendez-vous-disponibles/').json == {
        'err': 0,
        'collectivites': [
            {
                'id': 'col1',
                'nom': 'Saint-Didier',
                'url': 'https://saint-didier.fr/rdv2/',
                'logo_url': 'https://saint-didier.fr/logo.png',
                'created': '2023-03-03T10:00:00+00:00',
                'last_update': '2023-03-03T10:00:00+00:00',
                'lieux': [
                    {
                        'id': 'lieu1',
                        'latitude': 2.3,
                        'longitude': 1.5,
                        'nom': 'Mairie de Saint-Didier',
                        'numero_rue': '2 rue du four',
                        'code_postal': '99999',
                        'ville': 'Saint-Didier',
                        'nombre_de_jours_avec_rdv_disponibles': 1,
                        'nombre_de_pre_demandes_actives': 0,
                        'logo_url': 'https://saint-didier.fr/logo-mairie.png',
                        'url': 'https://saint-didier.fr/mairie/',
                        'rdv_url': 'https://saint-didier.fr/mairie/rdv/',
                        'gestion_url': 'https://saint-didier.fr/mairie/gestion/',
                        'annulation_url': 'https://saint-didier.fr/mairie/annulation/',
                        'last_gmmp': last_gmmp.isoformat(),
                    }
                ],
                'nombre_de_jours_avec_rdv_disponibles': 1,
                'nombre_de_lieux': 1,
                'nombre_de_pre_demandes_actives': 0,
            }
        ],
    }

    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '15:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                }
                            ],
                        }
                    ],
                }
            ]
        },
        status=400,
    )
    assert response.json == {
        'err': 1,
        'error': '["plage {\'date\': \'2023-03-20\', \'types_rdv\': [\'CNI\', '
        "'PASSPORT'], 'heure_debut': '15:00+02:00', 'heure_fin': "
        "'12:00+02:00', 'duree': 15, 'personnes': 1}: heure de fin "
        'inférieure à heure de début"]',
    }


@responses.activate
def test_predemandes(db, django_app):
    Config.set(Config.REQUEST_TO_ANTS_V2_AUTH_TOKEN, 'abcd')
    rac = Raccordement.objects.create(name='plateforme', apikey='abcd')
    col = Collectivite.objects.create(raccordement=rac)
    lieu = Lieu.objects.create(
        collectivite=col,
        nom='FooBar',
        source_id='1234',
        numero_rue='1',
        code_postal='99999',
        ville='FooBar',
        longitude=4.32,
        latitude=10.4,
    )
    django_app.set_authorization(('Basic', ('abcd', '')))

    document = {
        'ABCDE12345': {
            'status': 'validated',
            'appointments': [],
        },
        '1234567890': {
            'status': 'validated',
            'appointments': [],
        },
    }
    rdv_api_url = 'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status'
    responses.add(
        responses.GET,
        rdv_api_url,
        json=document,
        status=200,
        match=[
            responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            responses.matchers.query_string_matcher(
                f'application_ids=ABCDE12345&application_ids=1234567890&meeting_point_id={lieu.id}'
            ),
        ],
    )

    response = django_app.get('/api/chrono/predemandes/')
    assert response.json['err'] == 0
    assert response.json['data'] == []

    response = django_app.get(
        '/api/chrono/predemandes/',
        params=[('identifiant_predemande', 'ABCDE12345'), ('identifiant_predemande', '1234567890')],
    )
    assert response.json['err'] == 0
    assert response.json['data'] == []

    # with multiple identifiant_predemande
    document['ABCDE12345']['appointments'] = [
        {
            'management_url': 'https://rdvenmairie.fr/gestion/login?ants=83AHERZY8F&appointment_id=64594c435d7bfc0012fa8c87&canceled=true',
            'meeting_point': 'Mairie de Luisant',
            'appointment_date': '2023-09-20T09:00:11',
        }
    ]

    responses.replace(
        responses.GET,
        rdv_api_url,
        json=document,
        status=200,
        match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
    )

    response = django_app.get(
        '/api/chrono/predemandes/',
        params=[('identifiant_predemande', 'ABCDE12345'), ('identifiant_predemande', '1234567890')],
    )
    assert response.json == {
        'err': 0,
        'data': [
            {
                'appointment_date': '2023-09-20T09:00:11',
                'datetime': '2023-09-20T09:00:11',
                'identifiant_predemande': 'ABCDE12345',
                'management_url': 'https://rdvenmairie.fr/gestion/login?ants=83AHERZY8F&appointment_id=64594c435d7bfc0012fa8c87&canceled=true',
                'cancel_url': 'https://rdvenmairie.fr/gestion/login?ants=83AHERZY8F&appointment_id=64594c435d7bfc0012fa8c87&canceled=true',
                'meeting_point': 'Mairie de Luisant',
            }
        ],
    }

    responses.replace(
        responses.GET,
        rdv_api_url,
        json={},
        status=200,
        match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
    )
    response = django_app.get(
        '/api/chrono/predemandes/',
        params=[('identifiant_predemande', 'ABcDE12345'), ('identifiant_predemande', '1234567890')],
    )
    assert response.json == {
        'err': 1,
        'err_desc': 'Prédemande "ABCDE12345" inconnue. Prédemande "1234567890" inconnue.',
    }

    responses.replace(
        responses.GET,
        rdv_api_url,
        body=requests.RequestException('connection error'),
        match=[responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'})],
    )
    response = django_app.get(
        '/api/chrono/predemandes/',
        params=[('identifiant_predemande', 'ABCDE12345'), ('identifiant_predemande', '1234567890')],
    )
    assert response.json == {
        'err': 1,
        'err_desc': "RequestException('connection error')",
    }


def test_rendez_vous_disponibles_full(django_app, db, freezer):
    Raccordement.objects.create(name='plateforme', apikey='abcd')
    django_app.set_authorization(('Basic', ('abcd', '')))
    assert django_app.get('/api/chrono/rendez-vous-disponibles/').json == {
        'err': 0,
        'collectivites': [],
    }
    assert Collectivite.objects.count() == 0
    assert Lieu.objects.count() == 0
    assert Plage.objects.count() == 0
    assert RendezVous.objects.count() == 0

    django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'nom': 'Saint-Didier',
                    'url': 'https://saint-didier.fr/rdv/',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'nom': 'Mairie de Saint-Didier',
                            'numero_rue': '2 rue du four',
                            'code_postal': '99999',
                            'ville': 'Saint-Didier',
                            'longitude': 1.5,
                            'latitude': 2.3,
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-21',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-22',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                            ],
                            'rdvs': [
                                {
                                    'id': '12345aBCD1',
                                    'date': '2023-03-23T15:00:00+02:00',
                                },
                                {
                                    'id': '12345AbCD2',
                                    'date': '2023-03-24T15:00:00+02:00',
                                },
                                {
                                    'id': '12345ABcD3',
                                    'date': '2023-03-25T15:00:00+02:00',
                                },
                            ],
                        },
                        {
                            'id': 'lieu2',
                            'nom': 'Mairie annexe de Saint-Didier',
                            'numero_rue': '3 rue du four',
                            'code_postal': '99999',
                            'ville': 'Saint-Didier',
                            'longitude': 1.5,
                            'latitude': 2.3,
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-21',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-22',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                            ],
                            'rdvs': [
                                {
                                    'id': '12345ABCD4',
                                    'date': '2023-03-17T15:00:00+02:00',
                                },
                                {
                                    'id': '12345ABCD5',
                                    'date': '2023-03-18T15:00:00+02:00',
                                },
                                {
                                    'id': '12345ABCD6',
                                    'date': '2023-03-19T15:00:00+02:00',
                                },
                            ],
                        },
                    ],
                }
            ]
        },
    )

    assert Collectivite.objects.count() == 1
    assert Lieu.objects.count() == 2
    assert Plage.objects.count() == 12
    assert RendezVous.objects.count() == 6
    plage_last_update = set(Plage.objects.values_list('last_update', flat=True))
    rendez_vous_last_update = set(RendezVous.objects.values_list('last_update', flat=True))

    # advance clock to verify last_update times
    freezer.tick()
    django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'nom': 'Saint-Didier',
                    'url': 'https://saint-didier.fr/rdv/',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'nom': 'Mairie de Saint-Didier',
                            'numero_rue': '2 rue du four',
                            'code_postal': '99999',
                            'ville': 'Saint-Didier',
                            'longitude': 1.5,
                            'latitude': 2.3,
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-22',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                            ],
                            'rdvs': [
                                {
                                    'id': '12345ABCD1',
                                    'date': '2023-03-23T15:00:00+02:00',
                                },
                                {
                                    'id': '12345ABCD3',
                                    'date': '2023-03-25T15:00:00+02:00',
                                },
                            ],
                        },
                        {
                            'id': 'lieu2',
                            'nom': 'Mairie annexe de Saint-Didier',
                            'numero_rue': '3 rue du four',
                            'code_postal': '99999',
                            'ville': 'Saint-Didier',
                            'longitude': 1.5,
                            'latitude': 2.3,
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-22',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                            ],
                            'rdvs': [
                                {
                                    'id': '12345ABCD4',
                                    'date': '2023-03-17T15:00:00+02:00',
                                },
                                {
                                    'id': '12345ABCD6',
                                    'date': '2023-03-19T15:00:00+02:00',
                                },
                            ],
                        },
                    ],
                }
            ]
        },
    )

    assert Collectivite.objects.count() == 1
    assert Lieu.objects.count() == 2
    assert Plage.objects.count() == 12
    assert RendezVous.objects.count() == 6
    # check objects are not updated/created uselessly
    assert plage_last_update == set(Plage.objects.values_list('last_update', flat=True))
    assert rendez_vous_last_update == set(RendezVous.objects.values_list('last_update', flat=True))

    # advance clock to verify last_update times
    freezer.tick()
    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'nom': 'Saint-Didier',
                    'url': 'https://saint-didier.fr/rdv/',
                    'full': True,
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'nom': 'Mairie de Saint-Didier',
                            'numero_rue': '2 rue du four',
                            'code_postal': '99999',
                            'ville': 'Saint-Didier',
                            'longitude': 1.5,
                            'latitude': 2.3,
                            'full': True,
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-22',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                            ],
                            'rdvs': [
                                {
                                    'id': '12345ABCD1',
                                    'date': '2023-03-23T15:00:00+02:00',
                                },
                                {
                                    'id': '12345ABCD3',
                                    'date': '2023-03-25T15:00:00+02:00',
                                },
                            ],
                        },
                    ],
                }
            ]
        },
    )

    assert Collectivite.objects.count() == 1
    assert Lieu.objects.count() == 1
    assert Plage.objects.count() == 4
    assert RendezVous.objects.filter(canceled__isnull=True).count() == 2
    assert RendezVous.objects.filter(canceled__isnull=False).count() == 1
    assert set(
        RendezVous.objects.filter(canceled__isnull=True).values_list('identifiant_predemande', flat=True)
    ) == {'12345ABCD1', '12345ABCD3'}
    assert set(
        RendezVous.objects.filter(canceled__isnull=False).values_list('identifiant_predemande', flat=True)
    ) == {'12345ABCD2'}
    # check objects are not updated/created uselessly
    assert plage_last_update >= set(Plage.objects.values_list('last_update', flat=True))
    assert rendez_vous_last_update >= set(
        RendezVous.objects.filter(canceled__isnull=True).values_list('last_update', flat=True)
    )
    assert response.json == {
        'err': 0,
        'data': {
            'collectivites_created': 0,
            'collectivites_updated': 0,
            'lieux_created': 0,
            'lieux_updated': 0,
            'lieux_deleted': 1,
            'plage_created': 0,
            'plage_updated': 0,
            'plage_deleted': 8,
            'rdv_created': 0,
            'rdv_updated': 0,
            'rdv_deleted': 4,
        },
    }

    django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'nom': 'Saint-Didier',
                    'url': 'https://saint-didier.fr/rdv/',
                    'full': True,
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'nom': 'Mairie de Saint-Didier',
                            'numero_rue': '2 rue du four',
                            'code_postal': '99999',
                            'ville': 'Saint-Didier',
                            'longitude': 1.5,
                            'latitude': 2.3,
                            'full': True,
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-22',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                            ],
                            'rdvs': [
                                {
                                    'id': '12345aBCD1',
                                    'date': '2023-03-23T15:00:00+02:00',
                                },
                                {
                                    'id': '12345ABcd3',
                                    'date': '2023-03-25T15:00:00+02:00',
                                },
                            ],
                        },
                    ],
                }
            ]
        },
    )


def test_rendez_vous_disponibles_update_on_canceled(django_app, db, freezer):
    call_command('loaddata', 'fixtures/example1.json')
    raccordement = Raccordement.objects.get()
    raccordement.apikey = 'abcd'
    raccordement.save()

    django_app.set_authorization(('Basic', ('abcd', '')))

    assert RendezVous.objects.count() == 1
    assert RendezVous.objects.filter(canceled__isnull=True).count() == 1

    resp = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'saint-didier',
                    'lieux': [
                        {
                            'id': 'mairie',
                            'rdvs': [
                                {
                                    'id': 'abCD123456',
                                    'date': '2023-04-03T10:15:00Z',
                                    'annule': True,
                                },
                            ],
                        },
                    ],
                }
            ]
        },
    )

    assert resp.json == {
        'data': {
            'collectivites_created': 0,
            'collectivites_updated': 0,
            'lieux_created': 0,
            'lieux_deleted': 0,
            'lieux_updated': 0,
            'plage_created': 0,
            'plage_deleted': 0,
            'plage_updated': 0,
            # cancel existing rdv
            'rdv_created': 0,
            'rdv_deleted': 1,
            'rdv_updated': 0,
        },
        'err': 0,
    }

    assert RendezVous.objects.count() == 1
    assert RendezVous.objects.filter(canceled__isnull=False).count() == 1

    resp = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'saint-didier',
                    'lieux': [
                        {
                            'id': 'mairie',
                            'rdvs': [
                                {
                                    'id': 'abCD123456',
                                    'date': '2023-04-03T10:15:00Z',
                                },
                            ],
                        },
                    ],
                }
            ]
        },
    )

    assert resp.json == {
        'data': {
            'collectivites_created': 0,
            'collectivites_updated': 0,
            'lieux_created': 0,
            'lieux_deleted': 0,
            'lieux_updated': 0,
            'plage_created': 0,
            'plage_deleted': 0,
            'plage_updated': 0,
            # create new one with old identifian_predemande
            'rdv_created': 1,
            'rdv_deleted': 0,
            'rdv_updated': 0,
        },
        'err': 0,
    }
    assert RendezVous.objects.count() == 2
    assert RendezVous.objects.filter(canceled__isnull=True).count() == 1
    assert RendezVous.objects.filter(canceled__isnull=False).count() == 1

    resp = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'saint-didier',
                    'lieux': [
                        {
                            'id': 'mairie',
                            'rdvs': [
                                {
                                    'id': 'abCD123456',
                                    'date': '2023-04-03T10:15:00Z',
                                },
                            ],
                        },
                    ],
                }
            ]
        },
    )

    assert resp.json == {
        'data': {
            'collectivites_created': 0,
            'collectivites_updated': 0,
            'lieux_created': 0,
            'lieux_deleted': 0,
            'lieux_updated': 0,
            'plage_created': 0,
            'plage_deleted': 0,
            'plage_updated': 0,
            # nothing to do
            'rdv_created': 0,
            'rdv_deleted': 0,
            'rdv_updated': 0,
        },
        'err': 0,
    }
    assert RendezVous.objects.count() == 2
    assert RendezVous.objects.filter(canceled__isnull=True).count() == 1
    assert RendezVous.objects.filter(canceled__isnull=False).count() == 1


def test_naive_time(django_app, db, freezer):
    Raccordement.objects.create(name='plateforme', apikey='abcd')
    django_app.set_authorization(('Basic', ('abcd', '')))
    assert django_app.get('/api/chrono/rendez-vous-disponibles/').json == {
        'err': 0,
        'collectivites': [],
    }

    django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'nom': 'Saint-Didier',
                    'url': 'https://saint-didier.fr/rdv/',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'nom': 'Mairie de Saint-Didier',
                            'numero_rue': '2 rue du four',
                            'code_postal': '99999',
                            'ville': 'Saint-Didier',
                            'longitude': 1.5,
                            'latitude': 2.3,
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI'],
                                    'heure_debut': '08:00',
                                    'heure_fin': '09:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                            ],
                        },
                    ],
                }
            ]
        },
    )

    assert Plage.objects.count() == 1
    plage = Plage.objects.get()

    # checks that times from the plage update were interpreted as naive
    # datetime in the Europe/Paris timezone
    assert list(x for x, y in plage.available_time_slots()) == [
        datetime.datetime(2023, 3, 20, 8, 0, tzinfo=zoneinfo.ZoneInfo(key='Europe/Paris')),
        datetime.datetime(2023, 3, 20, 8, 15, tzinfo=zoneinfo.ZoneInfo(key='Europe/Paris')),
        datetime.datetime(2023, 3, 20, 8, 30, tzinfo=zoneinfo.ZoneInfo(key='Europe/Paris')),
        datetime.datetime(2023, 3, 20, 8, 45, tzinfo=zoneinfo.ZoneInfo(key='Europe/Paris')),
    ]


def test_plages_ordering(django_app, db, freezer):
    Raccordement.objects.create(name='plateforme', apikey='abcd')

    django_app.set_authorization(('Basic', ('abcd', '')))
    django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'nom': 'Saint-Didier',
                    'url': 'https://saint-didier.fr/rdv/',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'nom': 'Mairie de Saint-Didier',
                            'numero_rue': '2 rue du four',
                            'code_postal': '99999',
                            'ville': 'Saint-Didier',
                            'longitude': 1.5,
                            'latitude': 2.3,
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '12:00+02:00',
                                    'heure_fin': '13:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '09:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '10:00+02:00',
                                    'heure_fin': '11:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                            ],
                        },
                    ],
                }
            ]
        },
    )


def rdv_response(response):
    return {key: value for key, value in response.json.get('data', {}).items() if value > 0}


class TestUpdate:
    @pytest.fixture(autouse=True)
    def setup(self, django_app, db):
        Raccordement.objects.create(name='plateforme', apikey='abcd')
        django_app.set_authorization(('Basic', ('abcd', '')))

    def test_collectivite_and_lieu_update(self, django_app, db):
        response = django_app.post_json(
            '/api/chrono/rendez-vous-disponibles/',
            params={
                'collectivites': [
                    {
                        'id': 'col1',
                        'nom': 'Saint-Didier',
                        'url': 'https://saint-didier.fr/rdv/',
                        'logo_url': 'https://saint-didier.fr/logo.png',
                        'lieux': [
                            {
                                'id': 'lieu1',
                                'nom': 'Mairie de Saint-Didier',
                                'numero_rue': '2 rue du four',
                                'code_postal': '99999',
                                'ville': 'Saint-Didier',
                                'longitude': 1.5,
                                'latitude': 2.3,
                            }
                        ],
                    }
                ]
            },
        )

        assert rdv_response(response) == {
            'collectivites_created': 1,
            'lieux_created': 1,
        }

        response = django_app.post_json(
            '/api/chrono/rendez-vous-disponibles/',
            params={
                'collectivites': [
                    {
                        'id': 'col1',
                        'nom': 'Saint-François',
                        'url': 'https://saint-francois.fr/rdv/',
                        'logo_url': 'https://saint-didier.fr/logo.png',
                        'lieux': [
                            {
                                'id': 'lieu1',
                                'nom': 'Mairie de Saint-François',
                                'numero_rue': '2 rue du four',
                                'code_postal': '99999',
                                'ville': 'Saint-François',
                                'longitude': 1.5,
                                'latitude': 2.3,
                            }
                        ],
                    }
                ]
            },
        )

        assert rdv_response(response) == {
            'collectivites_updated': 1,
            'lieux_updated': 1,
        }


def test_annulation_rdv_full(django_app, db):
    Raccordement.objects.create(name='plateforme', apikey='abcd')
    django_app.set_authorization(('Basic', ('abcd', '')))

    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'nom': 'Saint-Didier',
                    'url': 'https://saint-didier.fr/rdv2/',
                    'logo_url': 'https://saint-didier.fr/logo.png',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'nom': 'Mairie de Saint-Didier',
                            'numero_rue': '2 rue du four',
                            'code_postal': '99999',
                            'ville': 'Saint-Didier',
                            'longitude': 1.5,
                            'latitude': 2.3,
                            'full': True,
                            'rdvs': [
                                {
                                    'id': '123456ABCD',
                                    'date': '2023-03-20T15:00:00+02:00',
                                },
                                {
                                    'id': '123456ABCD',
                                    'date': '2023-03-20T15:00:00+02:00',
                                },
                            ],
                        }
                    ],
                }
            ]
        },
    )
    assert rdv_response(response) == {
        'collectivites_created': 1,
        'lieux_created': 1,
        'rdv_created': 1,
    }
    assert RendezVous.objects.count() == 1
    assert RendezVous.objects.filter(canceled__isnull=True).count() == 1
    assert RendezVous.objects.filter(canceled__isnull=False).count() == 0

    # cancel existing rdv
    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'full': True,
                            'rdvs': [
                                {
                                    'id': '123456ABCD',
                                    'date': '2023-03-20T15:00:00+02:00',
                                    'annule': True,
                                }
                            ],
                        }
                    ],
                }
            ]
        },
    )
    assert rdv_response(response) == {'rdv_deleted': 1}
    assert RendezVous.objects.count() == 1
    assert RendezVous.objects.filter(canceled__isnull=True).count() == 0
    assert RendezVous.objects.filter(canceled__isnull=False).count() == 1

    # still send the cancellation, but create a new one with same id and date
    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'full': True,
                            'rdvs': [
                                {
                                    'id': '123456ABCD',
                                    'date': '2023-03-20T15:00:00+02:00',
                                },
                                {
                                    'id': '123456ABCD',
                                    'date': '2023-03-20T15:00:00+02:00',
                                    'annule': True,
                                },
                            ],
                        }
                    ],
                }
            ]
        },
    )
    assert rdv_response(response) == {'rdv_created': 1}
    assert RendezVous.objects.count() == 2
    assert RendezVous.objects.filter(canceled__isnull=True).count() == 1
    assert RendezVous.objects.filter(canceled__isnull=False).count() == 1

    # replay, nothing done
    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'full': True,
                            'rdvs': [
                                {
                                    'id': '123456ABCD',
                                    'date': '2023-03-20T15:00:00+02:00',
                                    'annule': True,
                                },
                                {
                                    'id': '123456ABCD',
                                    'date': '2023-03-20T15:00:00+02:00',
                                },
                            ],
                        }
                    ],
                }
            ]
        },
    )
    assert rdv_response(response) == {}
    assert RendezVous.objects.count() == 2
    assert RendezVous.objects.filter(canceled__isnull=True).count() == 1
    assert RendezVous.objects.filter(canceled__isnull=False).count() == 1

    # replay, order of rdvs changed, it shows that result does not depend on
    # order of rdvs
    response = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'full': True,
                            'rdvs': [
                                {
                                    'id': '123456ABCD',
                                    'date': '2023-03-20T15:00:00+02:00',
                                },
                                {
                                    'id': '123456ABCD',
                                    'date': '2023-03-20T15:00:00+02:00',
                                    'annule': True,
                                },
                            ],
                        }
                    ],
                }
            ]
        },
    )
    assert rdv_response(response) == {}
    assert RendezVous.objects.count() == 2
    assert RendezVous.objects.filter(canceled__isnull=True).count() == 1
    assert RendezVous.objects.filter(canceled__isnull=False).count() == 1


def test_rendez_vous_disponibles_database_is_locked(django_app, db, monkeypatch, settings):
    settings.ANTS_HUB_BUSY_BACKOFF = 0
    Raccordement.objects.create(name='plateforme', apikey='abcd')
    django_app.set_authorization(('Basic', ('abcd', '')))
    assert django_app.get('/api/chrono/rendez-vous-disponibles/').json == {
        'err': 0,
        'collectivites': [],
    }
    assert Collectivite.objects.count() == 0
    assert Lieu.objects.count() == 0
    assert Plage.objects.count() == 0
    assert RendezVous.objects.count() == 0

    old_method = RendezVousDisponibleView.handle_collectivite_payload
    call_count = 0

    def new_method(self, raccordement, payload):
        nonlocal call_count
        call_count += 1
        if call_count < 2:
            raise OperationalError('database is locked')
        return old_method(self, raccordement, payload)

    monkeypatch.setattr(RendezVousDisponibleView, 'handle_collectivite_payload', new_method)

    resp = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'nom': 'Saint-Didier',
                    'url': 'https://saint-didier.fr/rdv/',
                    'lieux': [
                        {
                            'id': 'lieu1',
                            'nom': 'Mairie de Saint-Didier',
                            'numero_rue': '2 rue du four',
                            'code_postal': '99999',
                            'ville': 'Saint-Didier',
                            'longitude': 1.5,
                            'latitude': 2.3,
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-21',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-22',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                            ],
                            'rdvs': [
                                {
                                    'id': '12345abcd1',
                                    'date': '2023-03-23T15:00:00+02:00',
                                },
                                {
                                    'id': '12345abcd2',
                                    'date': '2023-03-24T15:00:00+02:00',
                                },
                                {
                                    'id': '12345abcd3',
                                    'date': '2023-03-25T15:00:00+02:00',
                                },
                            ],
                        },
                        {
                            'id': 'lieu2',
                            'nom': 'Mairie annexe de Saint-Didier',
                            'numero_rue': '3 rue du four',
                            'code_postal': '99999',
                            'ville': 'Saint-Didier',
                            'longitude': 1.5,
                            'latitude': 2.3,
                            'plages': [
                                {
                                    'date': '2023-03-20',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-21',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                                {
                                    'date': '2023-03-22',
                                    'types_rdv': ['CNI', 'PASSPORT'],
                                    'heure_debut': '08:00+02:00',
                                    'heure_fin': '12:00+02:00',
                                    'duree': 15,
                                    'personnes': 1,
                                },
                            ],
                            'rdvs': [
                                {
                                    'id': '12345abcd4',
                                    'date': '2023-03-17T15:00:00+02:00',
                                },
                                {
                                    'id': '12345abcd5',
                                    'date': '2023-03-18T15:00:00+02:00',
                                },
                                {
                                    'id': '12345abcd6',
                                    'date': '2023-03-19T15:00:00+02:00',
                                },
                            ],
                        },
                    ],
                }
            ]
        },
    )

    assert call_count == 2
    assert resp.json['err'] == 0

    assert Collectivite.objects.count() == 1
    assert Lieu.objects.count() == 2
    assert Plage.objects.count() == 12
    assert RendezVous.objects.count() == 6


def test_rendez_vous_disponibles_database_is_busy(django_app, db, monkeypatch, settings):
    settings.ANTS_HUB_BUSY_BACKOFF = 0
    Raccordement.objects.create(name='plateforme', apikey='abcd')
    django_app.set_authorization(('Basic', ('abcd', '')))

    call_count = 0

    def new_method(self, raccordement, payload):
        nonlocal call_count
        call_count += 1
        raise OperationalError('database is locked')

    monkeypatch.setattr(RendezVousDisponibleView, 'handle_collectivite_payload', new_method)

    resp = django_app.post_json(
        '/api/chrono/rendez-vous-disponibles/',
        params={
            'collectivites': [
                {
                    'id': 'col1',
                    'nom': 'Saint-Didier',
                    'url': 'https://saint-didier.fr/rdv/',
                }
            ]
        },
    )

    assert call_count == 4
    assert resp.json['err'] == 1
    assert resp.json['error'] == 'busy'


class TestRdvStatus:
    @pytest.fixture(autouse=True)
    def setup(self, db, settings, django_app, caplog):
        Config.set(Config.REQUEST_TO_ANTS_V2_AUTH_TOKEN, 'abcd')
        raccordement = Raccordement.objects.create(name='plateforme', apikey='abcd')
        col = Collectivite.objects.create(raccordement=raccordement, nom='col1')
        Lieu.objects.create(collectivite=col, nom='lieu1', longitude=1, latitude=2, source_id='1')
        Lieu.objects.create(collectivite=col, nom='lieu2', longitude=1, latitude=2, source_id='2')
        django_app.set_authorization(('Basic', ('abcd', '')))
        caplog.set_level('INFO')

    @pytest.fixture
    def document(self):
        return {
            'ABCDE12345': {
                'status': 'validated',
                'appointments': [],
            },
            '1234567890': {
                'status': 'validated',
                'appointments': [],
            },
        }

    @responses.activate
    def test_ok(self, db, django_app, document, caplog):
        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json=document,
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )

        response = django_app.get(
            '/api/chrono/rdv-status/',
            params=[('identifiant_predemande', 'ABCDE12345'), ('identifiant_predemande', '1234567890')],
        )
        assert response.json == {
            'err': 0,
            'data': {
                'accept_rdv': True,
                'message': '',
                'ants_response': {
                    '1234567890': {'appointments': [], 'status': 'validated'},
                    'ABCDE12345': {'appointments': [], 'status': 'validated'},
                },
                'appointments': [],
            },
        }

        assert responses._default_mock.calls[0].request.url in [
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status?application_ids=ABCDE12345&application_ids=1234567890&meeting_point_id=1',
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status?application_ids=ABCDE12345&application_ids=1234567890&meeting_point_id=2',
        ]

        assert caplog.messages in [
            ['rdv-status(plateforme, lieu1) for application_ids ABCDE12345,1234567890 is ok.'],
            ['rdv-status(plateforme, lieu2) for application_ids ABCDE12345,1234567890 is ok.'],
        ]

    @responses.activate
    def test_nok(self, db, django_app, document, caplog):
        document['ABCDE12345']['appointments'] = [
            {
                'management_url': 'https://rdvenmairie.fr/gestion/login?ants=83AHERZY8F&appointment_id=64594c435d7bfc0012fa8c87&canceled=true',
                'meeting_point': 'Mairie de Luisant',
                'appointment_date': '2023-09-20T09:00:11',
            }
        ]

        responses.add(
            responses.GET,
            'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/status',
            json=document,
            status=200,
            match=[
                responses.matchers.header_matcher({'x-rdv-opt-auth-token': 'abcd'}),
            ],
        )

        response = django_app.get(
            '/api/chrono/rdv-status/',
            params=[('identifiant_predemande', 'ABCDE12345'), ('identifiant_predemande', '1234567890')],
        )
        assert response.json == {
            'err': 0,
            'data': {
                'accept_rdv': False,
                'ants_response': {
                    '1234567890': {'appointments': [], 'status': 'validated'},
                    'ABCDE12345': {
                        'appointments': [
                            {
                                'appointment_date': '2023-09-20T09:00:11',
                                'management_url': 'https://rdvenmairie.fr/gestion/login'
                                '?ants=83AHERZY8F&appointment_id=64594c435d7bfc0012fa8c87&canceled=true',
                                'meeting_point': 'Mairie ' 'de ' 'Luisant',
                            }
                        ],
                        'status': 'validated',
                    },
                },
                'message': 'Prédemande "ABCDE12345" déjà liée à un ou des rendez-vous.',
                'appointments': [
                    {
                        'appointment_date': '2023-09-20T09:00:11',
                        'identifiant_predemande': 'ABCDE12345',
                        'management_url': 'https://rdvenmairie.fr/gestion/login?ants=83AHERZY8F&appointment_id=64594c435d7bfc0012fa8c87&canceled=true',
                        'meeting_point': 'Mairie de Luisant',
                    }
                ],
            },
        }

        assert caplog.messages in [
            [
                'rdv-status(plateforme, lieu1) for application_ids ABCDE12345,1234567890 is nok: Prédemande "ABCDE12345" déjà liée à un ou des rendez-vous.'
            ],
            [
                'rdv-status(plateforme, lieu2) for application_ids ABCDE12345,1234567890 is nok: Prédemande "ABCDE12345" déjà liée à un ou des rendez-vous.'
            ],
        ]

    @responses.activate
    def test_invalid_format(self, db, django_app, document):
        response = django_app.get(
            '/api/chrono/rdv-status/',
            params=[('identifiant_predemande', 'ABCDE12345X'), ('identifiant_predemande', '1234567890')],
        )
        assert response.json == {
            'err': 0,
            'data': {
                'accept_rdv': False,
                'ants_response': {},
                'message': 'Identifiant de pré-demande "ABCDE12345X" invalide, il '
                'doit faire 10 caractères.',
            },
        }
