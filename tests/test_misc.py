# ANTS-Hub - Copyright (C) Entr'ouvert

import datetime
import re
import zoneinfo

import pytest

from ants_hub.urls import IsoDatetimeConverter


@pytest.mark.parametrize(
    'value,datetime',
    [
        (
            '2023-03-03T10:00:00+01:00',
            datetime.datetime(2023, 3, 3, 10, 0, tzinfo=zoneinfo.ZoneInfo('Europe/Paris')),
        ),
        (
            '2023-03-03T10:00:00+00:00',
            datetime.datetime(2023, 3, 3, 11, 0, tzinfo=zoneinfo.ZoneInfo('Europe/Paris')),
        ),
    ],
)
def test_iso_datetime_converter(value, datetime):
    converter = IsoDatetimeConverter()
    assert re.match(converter.regex, value)
    parsed = converter.to_python(value)
    assert parsed == datetime
    assert converter.to_url(datetime.astimezone(parsed.tzinfo)) == value
