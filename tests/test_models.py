# ANTS-Hub - Copyright (C) Entr'ouvert

import pytest
from django.core.exceptions import ValidationError

from ants_hub.data.models import Config, Raccordement, TypeDeRdv


def test_raccordement(db):
    raccordement = Raccordement.objects.create(name='Example')
    assert len(raccordement.apikey) >= 32
    old_apikey = raccordement.apikey
    raccordement.apikey = 'NEW'
    raccordement.clean()
    assert len(raccordement.apikey) >= 32 and raccordement.apikey != old_apikey

    raccordement.apikey = 'abcd'
    with pytest.raises(ValidationError):
        raccordement.clean()


def test_type_de_rdv():
    assert TypeDeRdv.CNI.ants_name == 'CNI'
    assert TypeDeRdv.PASSPORT.ants_name == 'PASSPORT'
    assert TypeDeRdv.CNI_PASSPORT.ants_name == 'CNI-PASSPORT'

    assert TypeDeRdv.CNI.label == 'CNI'
    assert TypeDeRdv.PASSPORT.label == 'Passeport'
    assert TypeDeRdv.CNI_PASSPORT.label == 'CNI et passeport'

    assert TypeDeRdv.from_ants_name('CNI') == TypeDeRdv.CNI
    assert TypeDeRdv.from_ants_name('PASSPORT') == TypeDeRdv.PASSPORT
    assert TypeDeRdv.from_ants_name('CNI-PASSPORT') == TypeDeRdv.CNI_PASSPORT


def test_config(db):
    Config.set(Config.REQUEST_FROM_ANTS_AUTH_TOKEN, 'xyz')
    assert Config.get(Config.REQUEST_FROM_ANTS_AUTH_TOKEN) == 'xyz'
    with pytest.raises(ValueError):
        Config.set('unknown', '1')
    assert 'authentification' in str(Config.objects.get())
