set -e

function silent() {
	LOG=`mktemp`
	"$@" >$LOG 2>&1 || (tail $LOG; rm $LOG; exit 1)
	rm $LOG
}

mkdir -p $TOX_WORK_DIR/spooler

silent ./manage.py migrate 3>&1
silent ./manage.py loaddata fixtures/admin.json 3>&1
silent ./manage.py loaddata fixtures/example1.json 3>&1
silent ./manage.py loaddata fixtures/config.json 3>&1

UWSGI_INI=$TOX_WORK_DIR/uwsgi.ini
cat >$UWSGI_INI <<EOF
[uwsgi]
plugin = python3
single-interpreter = true
module = ants_hub.wsgi:application
need-app = true
plugin = logfile
logger = file:$TOX_WORK_DIR/uwsgi.log
req-logger = file:/dev/fd/3
logformat-strftime = true
log-date = %%Y-%%m-%%d %%a %%H:%%M:%%S
log-format = %(ftime) INFO %(method) %(uri) (%(rsize) bytes, %(msecs) msecs, status %(status))
virtualenv = $TOX_ENV_DIR
auto-procname = true
procname-prefix-spaced = ants-hub
master = true
enable-threads = true
listen = 300
processes = 5
pidfile = $TOX_WORK_DIR/uwsgi.pid
http-socket = 127.0.0.1:9040
# spooler-python-import = ants_hub.spooler
# spooler = $TOX_WORK_DIR/spooler
python-autoreload = 1
EOF

echo


echo - uwsgi.ini: $UWSGI_INI
echo - pidfile: $TOX_WORK_DIR/uwsgi.pid
echo - uwsgi.log: $TOX_WORK_DIR/uwsgi.log
echo - spooler: $TOX_WORK_DIR/spooler
echo
echo
echo "     Open http://127.0.0.1:9040/ with login/password: admin / admin"

exec uwsgi $UWSGI_INI 3>&1
