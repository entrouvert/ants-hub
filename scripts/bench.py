import asyncio
import sys
import time

import aiohttp

requests = 0

url = (
    sys.argv[1]
    if len(sys.argv) > 1
    else (
        'http://127.0.0.1:9040/api/ants/availableTimeSlots?meeting_point_ids=1&meeting_point_ids=2&meeting_point_ids=3&start_date=2023-04-13&end_date=2023-04-17'
    )
)


async def make_request(i):
    global requests

    async with aiohttp.ClientSession() as client:
        for i in range(200):
            try:
                response = await client.get(
                    url,
                    headers={
                        'X-HUB-RDV-AUTH-TOKEN': '5c55f859e66e2f6dee41a55c6ebd8b04f9dd88f4b22dd451b2663797edff7fc3'
                    },
                )
            except Exception:
                pass
            else:
                requests += 1
                content = await response.json()
                assert isinstance(content, dict), content

        await client.close()


async def main():
    global requests

    async with asyncio.TaskGroup() as tg:
        for i in range(30):
            tg.create_task(make_request(i))


start = time.time()
asyncio.run(main())
duration = time.time() - start
print('Did', requests, 'requests in', duration, 'seconds RPS:', requests / duration)
