ANTS_HUB_BASE_URL = 'http://127.0.0.1:9040'
SECRET_KEY = '1234'
LOGGING = {
    'version': 1,
    'formatters': {
        'stream': {
            'format': '%(asctime)s %(levelname)s %(name)s: %(message)s',
            'datefmt': '%Y-%m-%d %a %H:%M:%S',
        },
    },
    'handlers': {
        'stream': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'formatter': 'stream',
            # fd3 is redirected to stdout in scripts/uwsgi.sh
            # to prevent logs to go to uwsgi log file.
            'filename': '/dev/fd/3',
        },
    },
    'loggers': {
        # even when debugging seeing SQL queries is too much, activate it
        # explicitly using DEBUG_DB
        'django.db': {
            'level': 'INFO',
            'handlers': [],
            'propagate': True,
        },
        'django': {
            'level': 'INFO',
            'handlers': [],
            'propagate': True,
        },
        '': {
            'handlers': ['stream'],
            'level': 'ERROR',
        },
    },
}
