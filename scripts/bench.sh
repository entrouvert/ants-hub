set -e

bash ./scripts/uwsgi.sh &
PID=$!

sleep 2
echo $PID
sleep 3


python3 ./scripts/bench.py

kill -KILL $PID
