#!/bin/bash
set -e

pylint -f parseable --rcfile ./scripts/pylint.rc "$@" | tee pylint.out; test $PIPESTATUS -eq 0
