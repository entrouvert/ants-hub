# ANTS-Hub - Copyright (C) Entr'ouvert


import importlib

from django.conf import settings
from django.urls.resolvers import URLPattern, URLResolver
from django.utils.decorators import decorator_from_middleware


class DecoratedURLPattern(URLPattern):
    def resolve(self, *args, **kwargs):
        result = super().resolve(*args, **kwargs)
        if result:
            result.func = self._decorate_with(result.func)
        return result


class DecoratedURLResolver(URLResolver):
    def resolve(self, *args, **kwargs):
        result = super().resolve(*args, **kwargs)
        if result:
            result.func = self._decorate_with(result.func)
        return result


def decorated_includes(func, includes, *args, **kwargs):
    urlconf_module, app_name, namespace = includes

    for item in getattr(urlconf_module, 'urlpatterns', urlconf_module):
        if isinstance(item, URLResolver):
            item.__class__ = DecoratedURLResolver
        else:
            item.__class__ = DecoratedURLPattern
        item._decorate_with = func

    return urlconf_module, app_name, namespace


def decorate_admin(includes):
    decorators = []
    for middleware_path in reversed(settings.ADMIN_MIDDLEWARE):
        module_name, class_name = middleware_path.rsplit('.', 1)
        module = importlib.import_module(module_name)
        middleware_class = getattr(module, class_name)
        decorators.append(decorator_from_middleware(middleware_class))

    def decorator(func):
        for deco in decorators:
            func = deco(func)
        return func

    return decorated_includes(decorator, includes)
