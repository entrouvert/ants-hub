# ANTS-Hub - Copyright (C) Entr'ouvert

import cProfile
import pstats
import time

from django.conf import settings


def profile_middleware(get_response):
    if not settings.PROFILE_FILE:
        return get_response

    start = time.time()
    pr = cProfile.Profile()

    def middleware(request):
        nonlocal start, pr

        pr.enable()
        try:
            return get_response(request)
        finally:
            pr.disable()
            # dump profile every 20 seconds
            if time.time() - start > 20:
                start = time.time()
                with open(settings.PROFILE_FILE, 'w') as fd:
                    sortby = pstats.SortKey.CUMULATIVE
                    ps = pstats.Stats(pr, stream=fd).sort_stats(sortby)
                    ps.print_stats()

    return middleware
