# ANTS-Hub - Copyright (C) Entr'ouvert

import os

from django.core.wsgi import get_wsgi_application

os.environ['DJANGO_SETTINGS_MODULE'] = 'ants_hub.settings'
application = get_wsgi_application()
