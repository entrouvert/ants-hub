# ANTS-Hub - Copyright (C) Entr'ouvert

from django.apps import AppConfig
from django.db.backends.signals import connection_created
from django.db.backends.sqlite3.base import DatabaseWrapper as Sqlite3DatabaseWrapper


class DataAppConfig(AppConfig):
    name = 'ants_hub.data'
    verbose_name = 'ANTS Hub Data model'

    def ready(self):
        connection_created.connect(self.initialize_sqlite3, sender=Sqlite3DatabaseWrapper)

    def initialize_sqlite3(self, sender, connection, **kwargs):
        with connection.cursor() as cur:
            cur.execute('PRAGMA journal_mode = WAL;')
            cur.execute('PRAGMA synchronous = normal;')
            cur.execute('PRAGMA temp_store = memory;')
            cur.execute('PRAGMA mmap_size = 30000000000;')
