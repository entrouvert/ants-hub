# ANTS-Hub - Copyright (C) Entr'ouvert

import django.contrib.admin.options
import django.contrib.admin.widgets
from django.conf import settings
from django.contrib import admin
from django.utils.html import format_html

from ants_hub.admin import site

from . import models


class UneditableMixin:
    def has_change_permission(self, request, obj=None):
        return settings.ANTS_HUB_ADMIN_ROLE in request.session.get('mellon_session', {}).get('roles', [])

    has_add_permission = has_change_permission


class ConfigAdmin(UneditableMixin, admin.ModelAdmin):
    list_display = ['key', 'value']


class RaccordementAdmin(admin.ModelAdmin):
    list_display = ['name', 'chrono_url', 'last_update']
    search_fields = ['name', 'apikey', 'notes']
    ordering = ['name']

    def chrono_url(self, instance):
        url = settings.ANTS_HUB_API_URL % instance.apikey
        return format_html('<a href="{0}">{0}</a>', url)

    chrono_url.short_description = 'SETTING_CHRONO_ANTS_HUB_URL'


class CollectiviteAdmin(UneditableMixin, admin.ModelAdmin):
    list_display = ['raccordement', 'nom', 'url', 'source_id', 'created', 'last_update']
    list_display_links = ['nom']
    search_fields = ['raccordement__name', 'nom']
    list_filter = ['raccordement']
    ordering = ['raccordement__name', 'nom']


class LieuAdmin(UneditableMixin, admin.ModelAdmin):
    list_display = ['collectivite', 'nom', 'created', 'last_update']
    list_display_links = ['nom']
    search_fields = ['collectivite__raccordement__name', 'collectivite__nom', 'nom']
    list_filter = ['collectivite__raccordement']
    ordering = ['collectivite__raccordement__name', 'collectivite__nom', 'nom']


class PlageAdmin(UneditableMixin, admin.ModelAdmin):
    date_hierarchy = 'date'
    list_display = ['date', 'lieu', 'type_de_rdv', 'personnes', 'horaires', 'created', 'last_update']
    search_fields = ['lieu__collectivite__raccordement__name', 'lieu__collectivite__nom', 'lieu__nom']
    list_filter = ['lieu__collectivite']
    ordering = [
        'date',
        'lieu__collectivite__raccordement__name',
        'lieu__collectivite__nom',
        'lieu__nom',
        'horaires',
        'type_de_rdv',
        'personnes',
    ]


class RendezVousAdmin(UneditableMixin, admin.ModelAdmin):
    date_hierarchy = 'date'
    list_display = ['date', 'lieu', 'identifiant_predemande', 'created', 'last_update']
    list_filter = ['lieu__collectivite']
    search_fields = ['identifiant_predemande']
    ordering = [
        'date',
        'lieu__collectivite__raccordement__name',
        'lieu__collectivite__nom',
        'lieu__nom',
        'identifiant_predemande',
    ]

    def short_identifiant_predemande(self, instance):
        return instance.identifiant_predemande[:4] + '…'

    short_identifiant_predemande.short_description = 'Identifiant de prédemande'


django.contrib.admin.options.FORMFIELD_FOR_DBFIELD_DEFAULTS.update(
    {
        models.CharField: {
            'widget': django.contrib.admin.widgets.AdminTextInputWidget,
        },
        models.URLField: {
            'widget': django.contrib.admin.widgets.AdminURLFieldWidget,
        },
    }
)

site.site_header = 'ANTS-Hub'
site.site_title = 'ANTS-Hub'
site.index_title = 'Administration'
site.register(models.Config, ConfigAdmin)
site.register(models.Raccordement, RaccordementAdmin)
site.register(models.Collectivite, CollectiviteAdmin)
site.register(models.Lieu, LieuAdmin)
site.register(models.Plage, PlageAdmin)
site.register(models.RendezVous, RendezVousAdmin)
