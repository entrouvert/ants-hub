# Generated by Django 3.2.19 on 2023-08-28 16:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('data', '0004_alter_plage_unique_together'),
    ]

    operations = [
        migrations.AddField(
            model_name='rendezvous',
            name='last_upload',
            field=models.DateTimeField(
                null=True, verbose_name="Dernière synchronisation avec l'ANTS", blank=True
            ),
        ),
    ]
