# ANTS-Hub - Copyright (C) Entr'ouvert

import datetime

from django.conf import settings
from django.urls import include, path, register_converter
from django.utils.timezone import is_aware

from . import admin, urls_utils, views


class IsoDatetimeConverter:
    regex = r'\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d(Z|\+\d\d:\d\d)'

    def to_python(self, value):
        dt = datetime.datetime.fromisoformat(value)
        if not is_aware(dt):
            raise ValueError
        return dt

    def to_url(self, value):
        assert is_aware(value)
        return value.isoformat()


register_converter(IsoDatetimeConverter, 'isodatetime')


urlpatterns = [
    path('', views.homepage),
    path('admin/', urls_utils.decorate_admin(admin.site.urls)),
    path('api/', include('ants_hub.api.urls')),
    path(
        'rdv/<slug:collectivite_slug>-<int:collectivite_pk>'
        '/<slug:lieu_slug>-<int:lieu_pk>/<isodatetime:date>/',
        views.rdv_redirect,
        name='rdv-redirect',
    ),
    path(
        'rdv/<slug:collectivite_slug>-<int:collectivite_pk>'
        '/<slug:lieu_slug>-<int:lieu_pk>/<isodatetime:date>/gestion/<slug:rdv_pk>/',
        views.gestion_redirect,
        name='gestion-redirect',
    ),
    path(
        'rdv/<slug:collectivite_slug>-<int:collectivite_pk>'
        '/<slug:lieu_slug>-<int:lieu_pk>/<isodatetime:date>/annulation/<slug:rdv_pk>/',
        views.annulation_redirect,
        name='annulation-redirect',
    ),
]

if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns += [
        path('saml/', urls_utils.decorate_admin(include('mellon.urls'))),
    ]
else:
    from django.contrib.staticfiles.urls import staticfiles_urlpatterns

    urlpatterns += staticfiles_urlpatterns()
