# ANTS-Hub - Copyright (C) Entr'ouvert

from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404

from ants_hub.data.models import Lieu, RendezVous


def homepage(request):
    return HttpResponseRedirect('admin/')


# collectivite_* args are ignored, they are only used for the readibility of the URL
def rdv_redirect(request, *, collectivite_pk, collectivite_slug, lieu_pk, lieu_slug, date):
    lieu = get_object_or_404(Lieu.objects.select_related('collectivite'), pk=lieu_pk)
    url = lieu.get_rdv_url()
    return HttpResponseRedirect(url)


# collectivite_*, lieu_* and date args are ignored, they are only used for the readibility of the URL
def gestion_redirect(request, *, collectivite_pk, collectivite_slug, lieu_pk, lieu_slug, date, rdv_pk):
    rendez_vous = get_object_or_404(RendezVous, pk=rdv_pk)
    url = rendez_vous.get_gestion_url()
    return HttpResponseRedirect(url)


def annulation_redirect(request, *, collectivite_pk, collectivite_slug, lieu_pk, lieu_slug, date, rdv_pk):
    rendez_vous = get_object_or_404(RendezVous, pk=rdv_pk)
    url = rendez_vous.get_annulation_url()
    return HttpResponseRedirect(url)
