# ANTS-Hub - Copyright (C) Entr'ouvert

from django.conf import settings
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.utils.module_loading import autodiscover_modules
from django.views.decorators.cache import never_cache

if 'mellon' in settings.INSTALLED_APPS:
    import mellon.views as mellon_views  # pylint: disable=import-error
else:
    mellon_views = None


class AdminSite(admin.AdminSite):
    @method_decorator(never_cache)
    def login(self, request, extra_context=None):
        if mellon_views:
            return HttpResponseRedirect('/saml/login/')
        return super().login(request, extra_context=extra_context)

    @method_decorator(never_cache)
    def logout(self, request, extra_context=None):
        if mellon_views:
            return HttpResponseRedirect('/saml/logout/')
        return super().logout(request, extra_context=extra_context)


site = AdminSite()
if not mellon_views:
    site.register(get_user_model(), UserAdmin)
autodiscover_modules('admin', register_to=site)
