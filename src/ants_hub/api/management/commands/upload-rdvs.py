# ANTS-Hub - Copyright (C) Entr'ouvert

from django.core.management.base import BaseCommand

from ants_hub.api.ants import upload_rdvs


class Command(BaseCommand):
    help = 'Télécharger les rdvs associés à des numéros de pré-demande'

    def handle(self, *args, **options):
        upload_rdvs()
