# ANTS-Hub - Copyright (C) Entr'ouvert

from django.urls import path

from .views import ants, chrono

# ANTS API
urlpatterns = [
    path('ants/getManagedMeetingPoints', ants.get_managed_meeting_points),
    path('ants/availableTimeSlots', ants.available_time_slots),
    path('ants/searchApplicationIds', ants.search_application_ids),
]

# Chrono API
urlpatterns += [
    path('chrono/ping/', chrono.ping),
    path('chrono/rendez-vous-disponibles/', chrono.rendez_vous_disponibles),
    path('chrono/predemandes/', chrono.predemandes),
    path('chrono/rdv-status/', chrono.rdv_status),
]
