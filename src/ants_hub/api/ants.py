# ANTS-Hub - Copyright (C) Entr'ouvert

import logging
import re

import requests
from django.db import OperationalError
from django.db.models import F, Q
from django.db.transaction import atomic

from ants_hub.data.models import Config, RendezVous
from ants_hub.timezone import localtime, now

logger = logging.getLogger('ants_hub.api.ants')


class ANTSError(Exception):
    pass


class ApplicationIdNotFound(ANTSError):
    pass


def get_api_optimisation_auth_token():
    return Config.get(Config.REQUEST_TO_ANTS_V2_AUTH_TOKEN)


def get_api_optimisation_url():
    return Config.get(
        Config.REQUEST_TO_ANTS_V2_BASE_URL, 'https://api-coordination.rendezvouspasseport.ants.gouv.fr/api/'
    )


IDENTIFIANT_PREDEMANDE_RE = re.compile(r'^[A-Z0-9]{10}$')


class APIDoublon:
    def __init__(self):
        self.auth_token = get_api_optimisation_auth_token()
        if self.auth_token is None:
            raise ANTSError('REQUEST_TO_ANTS_V2_AUTH_TOKEN not configured')
        self.base_url = get_api_optimisation_url()

    def request(self, method, endpoint, **kwargs):
        try:
            response = requests.request(
                method,
                self.base_url + endpoint,
                headers={'x-rdv-opt-auth-token': self.auth_token},
                timeout=10,
                **kwargs,
            )
            response.raise_for_status()
            return response.json()
        except requests.HTTPError as e:
            if e.response.status_code == 404 and b'Application not found in database' in e.response.content:
                raise ApplicationIdNotFound()
            try:
                value = e.response.json()
            except ValueError:
                try:
                    value = e.response.text[:128]
                except ValueError:
                    value = e.response.content[:128]
            raise ANTSError(
                str(e),
                value,
            )
        except (requests.RequestException, ValueError, KeyError, TypeError) as e:
            raise ANTSError(repr(e))

    def create_rdv(self, rdv):
        identifiant_predemande = rdv.identifiant_predemande.upper()
        params = [
            ('application_id', identifiant_predemande),
            ('management_url', rdv.get_gestion_url_for_ants()),
            ('meeting_point', rdv.lieu.nom),
            ('meeting_point_id', str(rdv.lieu.id)),
            ('appointment_date', localtime(rdv.date).strftime('%Y-%m-%d %H:%M:%S')),
        ]
        response = self.request(method='POST', endpoint='appointments', params=params)
        try:
            if not response['success']:
                raise RuntimeError('creation failed, success is not true')
        except Exception as e:
            raise ANTSError(str(e), {'response': response})

    def delete_rdv(self, rdv):
        identifiant_predemande = rdv.identifiant_predemande.upper()
        params = [
            ('application_id', identifiant_predemande),
            ('meeting_point', rdv.lieu.nom),
            ('meeting_point_id', str(rdv.lieu.id)),
            ('appointment_date', localtime(rdv.date).strftime('%Y-%m-%d %H:%M:%S')),
        ]
        response = self.request(method='DELETE', endpoint='appointments', params=params)
        try:
            if not bool(response['rowcount']):
                raise RuntimeError('deletion failed, rowcount is zero')
        except Exception as e:
            raise ANTSError(str(e), {'response': response})

    def statuses(self, application_ids, meeting_point_id=None):
        params = [('application_ids', application_id) for application_id in application_ids]
        if meeting_point_id:
            params.append(('meeting_point_id', meeting_point_id))
        data = self.request(method='GET', endpoint='status', params=params)
        if not isinstance(data, dict):
            raise ANTSError('response is not a dict', data)
        return data

    def status(self, application_id, meeting_point_id=None):
        return self.statuses([application_id], meeting_point_id=meeting_point_id).get(application_id, {})

    def existing_rdv(self, rdv):
        meeting_point_id = str(rdv.lieu.id)
        identifiant_predemande = rdv.identifiant_predemande.upper()
        status_response = self.status(identifiant_predemande, meeting_point_id=meeting_point_id)
        status = status_response.get('status')
        appointments = status_response.get('appointments', [])

        ref = [
            ('meeting_point', rdv.lieu.nom),
            ('appointment_date', localtime(rdv.date).strftime('%Y-%m-%dT%H:%M:%S')),
        ]

        matching_appointments = []
        for appointment in appointments:
            for key, value in ref:
                if appointment[key] != value:
                    break
            else:
                matching_appointments.append(appointment)
        return status, matching_appointments


def push_rdv(rdv):
    identifiant_predemande = rdv.identifiant_predemande.upper()
    if not IDENTIFIANT_PREDEMANDE_RE.match(identifiant_predemande):
        return 'ignored because identifiant_predemande is malformed'

    api_doublon = APIDoublon()

    status, existing = api_doublon.existing_rdv(rdv)

    if status == 'unknown':
        return 'ignored because identifiant_predemande is unknown'

    if rdv.canceled:
        if not existing:
            # Tout est déjà bon, on ne fait rien
            return False
        api_doublon.delete_rdv(rdv)
        return 'deleted'
    else:
        if len(existing) > 1:
            # Il y a des doublons pour la même référence meeting_point/date on supprime tout
            api_doublon.delete_rdv(rdv)
        elif existing:
            if existing[0]['management_url'] != rdv.get_gestion_url_for_ants():
                # L'URL de gestion a changé, on supprime le RdV existant
                api_doublon.delete_rdv(rdv)
            else:
                # Tout est déjà bon, on ne fait rien
                return False
        try:
            api_doublon.create_rdv(rdv)
        except ApplicationIdNotFound:
            return 'ignored because identifiant_predemande does not exist'
        return 'created'


def get_rdvs_to_upload():
    return RendezVous.objects.filter(
        Q(last_upload__isnull=True) | Q(last_upload__lt=F('last_update'))
    ).select_related('lieu')


def upload_rdvs():
    # get never uploaded rdvs and rdv uploaded which changed (cancelled)
    rdvs = get_rdvs_to_upload()
    start = now()
    for rdv in rdvs.distinct():
        try:
            with atomic():
                try:
                    rdv = rdvs.select_for_update(of=('self',)).get(pk=rdv.pk)
                except RendezVous.DoesNotExist:
                    continue
                try:
                    result = push_rdv(rdv)
                    if result:
                        # si result == False, c'est un rejeu inutile, on ne log rien, mais on met à jour last_upload
                        logger.info(f'{result} rdv(%s) of lieu %s', rdv, rdv.lieu)
                    rdv.last_upload = start
                    rdv.save(update_fields=['last_upload'])
                except ANTSError as e:
                    logger.warning('unable to push rdv(%s) of lieu %s: %r', rdv, rdv.lieu, e)
        except OperationalError:
            pass


def get_status_of_predemandes(identifiant_predemandes: list, meeting_point_id: None | str = None):
    api_doublon = APIDoublon()

    predemandes_statuses = api_doublon.statuses(identifiant_predemandes, meeting_point_id=meeting_point_id)

    valid = True
    msg = []
    appointments = []
    for identifiant_predemande in identifiant_predemandes:
        if identifiant_predemande not in predemandes_statuses:
            msg.append(f'Prédemande "{identifiant_predemande}" inconnue.')
            valid = False
            continue
        predemande_state = predemandes_statuses[identifiant_predemande]
        if not isinstance(predemande_state, dict):
            raise ANTSError(f'application_id state is not a dict: {predemandes_statuses!r}')
        if predemande_state.get('status') != 'validated':
            msg.append(f'Prédemande "{identifiant_predemande}" inconnue, expirée ou déjà consommée.')
            valid = False
            continue
        if predemande_state.get('appointments', []):
            appointments.extend(
                [
                    {'identifiant_predemande': identifiant_predemande, **appointment}
                    for appointment in predemande_state['appointments']
                ]
            )
            msg.append(f'Prédemande "{identifiant_predemande}" déjà liée à un ou des rendez-vous.')
            valid = False
            continue
    return valid, ' '.join(msg), predemandes_statuses, appointments
