# ANTS-Hub - Copyright (C) Entr'ouvert

import base64
import binascii
import collections
import datetime
import functools
import json
import logging
import os
import time

import jsonschema
from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import OperationalError
from django.db.models import Count
from django.db.transaction import atomic
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from ants_hub.api.ants import IDENTIFIANT_PREDEMANDE_RE, ANTSError, get_status_of_predemandes
from ants_hub.data.models import (
    Collectivite,
    Horaire,
    HoraireList,
    Lieu,
    Plage,
    Raccordement,
    RendezVous,
    TypeDeRdv,
)
from ants_hub.interval import IntervalSet
from ants_hub.timezone import now

logger = logging.getLogger('ants_hub.api.chrono')


with open(os.path.join(os.path.dirname(__file__), '..', 'static', 'schemas', 'rdv-disponibles.json')) as fd:
    RENDEZ_VOUS_DISPONIBLES_SCHEMA = json.load(fd)


class Http401(JsonResponse):
    def __init__(self, message):
        super().__init__({'err': message}, status=401, safe=False)
        self['WWW-Authenticate'] = 'Basic'


def authenticate(func):
    @functools.wraps(func)
    def wrapper(request, *args, **kwargs):
        header = request.headers.get('Authorization', '').encode()
        if not header:
            logger.warning('authentication failed, missing Authorization header')
            return Http401('Missing Authorization header')
        auth = header.split(maxsplit=1)
        if not auth or auth[0].lower() not in [b'basic', b'bearer'] or len(auth) != 2:
            logger.warning('authentication failed, invalid Authorization header')
            return Http401('Invalid Authorization header')

        if auth[0].lower() == b'basic':
            try:
                auth_decoded = base64.b64decode(auth[1]).decode()
            except (TypeError, UnicodeDecodeError, binascii.Error):
                logger.warning('authentication failed, invalid Authorization header')
                return Http401('Invalid Authorization header')

            apikey = auth_decoded.split(':', 1)[0]
        else:
            assert auth[0].lower() == b'bearer'
            try:
                apikey = auth[1].decode()
            except UnicodeDecodeError:
                logger.warning('authentication failed, invalid Authorization header')
                return Http401('Invalid Authorization header')
        raccordement = Raccordement.objects.get_by_apikey(apikey)

        if not raccordement:
            logger.warning('authentication failed, unknown API key "%s"', apikey)
            return Http401('Unknown API key')
        request.raccordement = raccordement
        return func(request, *args, **kwargs)

    return wrapper


@authenticate
def ping(request):
    logger.info('ping received')
    return JsonResponse({'err': 0})


class RendezVousDisponibleView(View):
    def get(self, request):
        collectivites_data = []
        for collectivite in request.raccordement.collectivites.prefetch_related('lieux').order_by('created'):
            lieux_data = []
            collectivite_data = {
                'id': collectivite.source_id,
                'nom': collectivite.nom,
                'url': collectivite.url,
                'created': collectivite.created.isoformat(),
                'last_update': collectivite.created.isoformat(),
                'lieux': lieux_data,
                'nombre_de_lieux': collectivite.lieux.count(),
                'nombre_de_pre_demandes_actives': RendezVous.objects.filter(
                    date__gte=now(),
                    lieu__collectivite=collectivite,
                    canceled__isnull=True,
                ).count(),
                'nombre_de_jours_avec_rdv_disponibles': Plage.objects.filter(
                    lieu__collectivite=collectivite, date__gte=now().date()
                ).aggregate(Count('date'))['date__count'],
            }
            for key in ['logo_url', 'rdv_url', 'gestion_url', 'annulation_url']:
                if getattr(collectivite, key, None):
                    collectivite_data[key] = getattr(collectivite, key)
            for lieu in collectivite.lieux.order_by('created'):
                lieu_data = {
                    'id': lieu.source_id,
                    'nom': lieu.nom,
                    'numero_rue': lieu.numero_rue,
                    'code_postal': lieu.code_postal,
                    'ville': lieu.ville,
                    'longitude': lieu.longitude,
                    'latitude': lieu.latitude,
                    'nombre_de_pre_demandes_actives': lieu.rdvs.filter(
                        date__gte=now(), canceled__isnull=True
                    ).count(),
                    'nombre_de_jours_avec_rdv_disponibles': lieu.plages.filter(
                        date__gte=now().date()
                    ).aggregate(Count('date'))['date__count'],
                    'last_gmmp': lieu.last_gmmp and lieu.last_gmmp.isoformat(),
                }
                for key in ['url', 'logo_url', 'rdv_url', 'gestion_url', 'annulation_url']:
                    if getattr(lieu, key, None):
                        lieu_data[key] = getattr(lieu, key)
                lieux_data.append(lieu_data)
            collectivites_data.append(collectivite_data)

        logger.info(
            'rendez-vous-disponibles(%s) returned %d collectivites',
            request.raccordement,
            len(collectivites_data),
        )
        return JsonResponse(
            {
                'err': 0,
                'collectivites': collectivites_data,
            }
        )

    collectivites_created = 0
    collectivites_updated = 0
    lieux_created = 0
    lieux_updated = 0
    lieux_deleted = 0
    plage_created = 0
    plage_updated = 0
    plage_deleted = 0
    rdv_created = 0
    rdv_updated = 0
    rdv_deleted = 0

    def handle_rdv_payload(self, lieu, rdv):
        # cannot fail, as JSON schema is already validated
        rdv = rdv.copy()
        identifiant_predemande = rdv.pop('id').strip().upper()
        date = datetime.datetime.fromisoformat(rdv.pop('date'))
        annule = bool(rdv.pop('annule', False))
        rdv.setdefault('gestion_url', '')
        rdv.setdefault('annulation_url', '')

        if not IDENTIFIANT_PREDEMANDE_RE.match(identifiant_predemande):
            logger.warning(
                'rendez-vous-disponibles(%s) identifiant_predemande invalid "%s" on lieu "%s"',
                self.request.raccordement,
                identifiant_predemande,
                lieu,
            )
            return []

        if annule:
            timestamp = now()
            rdvs = list(
                lieu.rdvs.filter(
                    identifiant_predemande=identifiant_predemande, date=date, canceled__isnull=True
                ).select_for_update()
            )
            count = lieu.rdvs.filter(
                identifiant_predemande=identifiant_predemande, date=date, canceled__isnull=True
            ).update(canceled=timestamp, last_update=timestamp)
            self.rdv_deleted += count
            return rdvs

        rendez_vous = lieu.rdvs.filter(
            identifiant_predemande=identifiant_predemande, date=date, canceled__isnull=True, **rdv
        ).first()
        if rendez_vous is None:
            rendez_vous, created = lieu.rdvs.update_or_create(
                identifiant_predemande=identifiant_predemande, date=date, canceled=None, defaults=rdv
            )
            if created:
                self.rdv_created += 1
            else:
                self.rdv_updated += 1
        return [rendez_vous]

    def handle_rdvs_payload(self, lieu, rdvs, full=False):
        rdv_pks = set()
        # sort rdvs by the annule property, so if the same rdv is created and
        # canceled, we just update it
        rdvs.sort(key=lambda rdv: rdv.get('annule', False))
        seen = set()
        for rdv in rdvs:
            identifiant_predemande = rdv['id'].strip().upper()
            date = datetime.datetime.fromisoformat(rdv['date'])
            key = (date, identifiant_predemande)
            # prevent duplicate creation of rdv canceled and created
            if key in seen:
                continue
            seen.add(key)
            updated_rdvs = self.handle_rdv_payload(lieu, rdv)
            if full and updated_rdvs:
                rdv_pks.update(updated_rdv.pk for updated_rdv in updated_rdvs)
        if full:
            timestamp = now()
            count = (
                lieu.rdvs.exclude(pk__in=rdv_pks)
                .filter(canceled__isnull=True)
                .update(canceled=timestamp, last_update=timestamp)
            )
            self.rdv_deleted += count
        return lieu

    def handle_plages_payload(self, lieu, plages, full=False):
        by_date_and_type_and_personnes_and_duree = collections.defaultdict(IntervalSet)
        day_is_full = set()
        for plage in plages:
            try:
                date = datetime.date.fromisoformat(plage['date'])
                types_rdv = set()
                # by JSON schema, types_rdv is never empty and always filled
                # with expected values
                for typ in plage['types_rdv']:
                    types_rdv.add(TypeDeRdv.from_ants_name(typ))
                personnes = plage.get('personnes', 1)
                if not plage.get('heure_debut'):
                    day_is_full.update((date, t, personnes, 0) for t in types_rdv)
                    continue
                heure_debut = datetime.time.fromisoformat(plage['heure_debut'])
                heure_fin = datetime.time.fromisoformat(plage['heure_fin'])
                heure_debut = heure_debut.replace(tzinfo=None)
                heure_fin = heure_fin.replace(tzinfo=None)
                duree = int(plage['duree'])
                if heure_fin < heure_debut:
                    raise ValueError('heure de fin inférieure à heure de début')
                if datetime.datetime.combine(date, heure_fin) - datetime.datetime.combine(
                    date, heure_debut
                ) < datetime.timedelta(minutes=duree):
                    raise ValueError('différence heure de début et de fin inférieure à la durée')
                for t in types_rdv:
                    by_date_and_type_and_personnes_and_duree[(date, t, personnes, duree)].add(
                        Horaire(heure_debut, heure_fin)
                    )
            except ValueError as e:
                raise ValidationError('plage %s: %s' % (plage, e))

        plage_pks = set()
        for x in set(by_date_and_type_and_personnes_and_duree) | day_is_full:
            date, type_rdv, personnes, duree = x
            if (date, type_rdv, personnes, 0) in day_is_full:
                _, count_by_model = lieu.plages.filter(
                    date=date, type_de_rdv=type_rdv, personnes=personnes
                ).delete()
                self.plage_deleted += count_by_model.get('data.Plage', 0)
                continue
            intervals = by_date_and_type_and_personnes_and_duree.get(x)
            horaires = HoraireList()
            for interval in intervals:
                # intervals cannot intersect by definition of IntervalSet
                horaires.append(Horaire(start=interval.begin, end=interval.end))
            try:
                plage = lieu.plages.get(
                    date=date, type_de_rdv=type_rdv, personnes=personnes, horaires=horaires, duree=duree
                )
            except Plage.DoesNotExist:
                plage, created = lieu.plages.update_or_create(
                    date=date,
                    type_de_rdv=type_rdv,
                    personnes=personnes,
                    duree=duree,
                    defaults={'horaires': horaires, 'duree': duree},
                )
                if created:
                    self.plage_created += 1
                else:
                    self.plage_updated += 1
            if full:
                plage_pks.add(plage.pk)
        if full:
            _, count_by_model = lieu.plages.exclude(pk__in=plage_pks).delete()
            self.plage_deleted += count_by_model.get('data.Plage', 0)

    def handle_lieu_payload(self, collectivite, payload):
        payload = payload.copy()
        source_id = payload.pop('id')
        plages = payload.pop('plages', [])
        rdvs = payload.pop('rdvs', [])
        full = payload.pop('full', False)

        try:
            lieu = collectivite.lieux.get(source_id=source_id)
        except Lieu.DoesNotExist:
            # new
            lieu = collectivite.lieux.create(source_id=source_id, **payload)
            self.lieux_created += 1
        else:
            # existing
            updated = False
            for key, value in payload.items():
                if getattr(lieu, key) != value:
                    updated = True
                    setattr(lieu, key, value)
            if updated:
                lieu.save()
                self.lieux_updated += 1

        self.handle_plages_payload(lieu, plages, full=full)
        self.handle_rdvs_payload(lieu, rdvs, full=full)
        return lieu

    def handle_collectivite_payload(self, raccordement, payload):
        payload = payload.copy()
        source_id = payload.pop('id')
        lieux_payload = payload.pop('lieux', [])
        full = payload.pop('full', False)

        try:
            # fast path, instance exactly match
            collectivite = raccordement.collectivites.get(source_id=source_id)
        except Collectivite.DoesNotExist:
            # new
            collectivite = raccordement.collectivites.create(source_id=source_id, **payload)
            self.collectivites_created += 1
        else:
            # existing
            updated = False
            for key, value in payload.items():
                if getattr(collectivite, key) != value:
                    updated = True
                    setattr(collectivite, key, value)
            if updated:
                collectivite.save()
                self.collectivites_updated += 1

        # handle lieux
        lieu_pks = set()
        for lieu_payload in lieux_payload:
            lieu = self.handle_lieu_payload(collectivite, lieu_payload)
            if full:
                lieu_pks.add(lieu.pk)
        if full:
            _, count_by_model = collectivite.lieux.exclude(pk__in=lieu_pks).delete()
            self.lieux_deleted += count_by_model.get('data.Lieu', 0)
            self.plage_deleted += count_by_model.get('data.Plage', 0)
            self.rdv_deleted += count_by_model.get('data.RendezVous', 0)

    def post(self, request):
        try:
            payload = json.loads(request.body)
        except ValueError:
            return JsonResponse({'err': 'invalid-json'}, status=400)
        try:
            jsonschema.validate(payload, RENDEZ_VOUS_DISPONIBLES_SCHEMA)
        except jsonschema.ValidationError as e:
            return JsonResponse({'err': 'invalid-json', 'detail': e.message}, status=400)
        try:
            i = 0
            while True:
                i += 1
                try:
                    with atomic():
                        # prevent concurrent updates to the data of the same raccordement
                        request.raccordement.lock()
                        for collectivite in payload.get('collectivites', []):
                            self.handle_collectivite_payload(request.raccordement, collectivite)
                    break
                except OperationalError as e:
                    if 'database is locked' not in str(e):
                        raise
                    if i > 3:
                        logger.warning(
                            'rendez-vous-disponibles(%s) server is too busy "%s"', request.raccordement, e
                        )
                        return JsonResponse(
                            {
                                'err': 1,
                                'error': 'busy',
                            },
                        )
                    time.sleep(settings.ANTS_HUB_BUSY_BACKOFF * 2**i)
        except (ValueError, ValidationError) as e:
            logger.warning('rendez-vous-disponibles(%s) received bad request "%s"', request.raccordement, e)
            return JsonResponse(
                {
                    'err': 1,
                    'error': str(e),
                },
                status=400,
            )
        actions = []
        result = {}
        for action in ['created', 'updated', 'deleted']:
            parts = []
            for model in ['collectivites', 'lieux', 'plage', 'rdv']:
                name = f'{model}_{action}'
                if name == 'collectivites_deleted':
                    continue
                result[name] = count = getattr(self, name)
                if count:
                    parts.append(f'{count} {model}')
            if parts:
                actions.append(f'{action} {", ".join(parts)}')
        if actions:
            logger.info('rendez-vous-disponibles(%s) %s', request.raccordement, '; '.join(actions))
        else:
            logger.debug('rendez-vous-disponibles(%s) nothing done.', request.raccordement)
        return JsonResponse(
            {
                'err': 0,
                'data': result,
            }
        )


rendez_vous_disponibles = csrf_exempt(authenticate(RendezVousDisponibleView.as_view()))


class PredemandesView(View):
    def get_autres_demandes(self, identifiant_predemandes):
        identifiant_predemandes = list(filter(IDENTIFIANT_PREDEMANDE_RE.match, identifiant_predemandes))
        try:
            accept_rdv, message, dummy, appointments = get_status_of_predemandes(
                identifiant_predemandes=identifiant_predemandes,
                # attach the status request to a random lieu of the raccordement
                meeting_point_id=self.request.raccordement.get_random_meeting_point_id(),
            )
        except ANTSError as e:
            return None, JsonResponse(
                {
                    'err': 1,
                    'err_desc': str(e),
                }
            )
        for appointment in appointments:
            appointment['datetime'] = appointment.get('appointment_date')
            appointment['cancel_url'] = appointment.get('management_url')
        if not accept_rdv and not appointments:
            return None, JsonResponse(
                {
                    'err': 1,
                    'err_desc': message,
                }
            )
        return appointments, None

    def get(self, request):
        identifiant_predemandes = [
            part.strip().upper() for part in request.GET.getlist('identifiant_predemande') if part.strip()
        ]
        if not identifiant_predemandes:
            return JsonResponse({'err': 0, 'data': []})
        autres_demandes, error_response = self.get_autres_demandes(identifiant_predemandes)
        if error_response is not None:
            return error_response

        return JsonResponse(
            {
                'err': 0,
                'data': autres_demandes,
            }
        )


predemandes = csrf_exempt(authenticate(PredemandesView.as_view()))


class RdvStatus(View):
    def get(self, request):
        identifiant_predemandes = [
            part.strip().upper() for part in request.GET.getlist('identifiant_predemande') if part.strip()
        ]
        if not identifiant_predemandes:
            return JsonResponse(
                {
                    'err': 0,
                    'data': {
                        'accept_rdv': True,
                    },
                }
            )

        msg = []
        for identifiant_predemande in identifiant_predemandes:
            if not IDENTIFIANT_PREDEMANDE_RE.match(identifiant_predemande):
                msg.append(
                    f'Identifiant de pré-demande "{identifiant_predemande}" invalide, il doit faire 10 caractères.'
                )
        if msg:
            logger.info(
                'rdv-status(%s) for application_ids %s is nok: %s',
                request.raccordement,
                ','.join(identifiant_predemandes),
                ' '.join(msg),
            )
            return JsonResponse(
                {
                    'err': 0,
                    'data': {
                        'accept_rdv': False,
                        'message': ' '.join(msg),
                        'ants_response': {},
                    },
                }
            )

        random_meeting_point = request.raccordement.get_random_meeting_point()
        random_meeting_point_id = random_meeting_point.id if random_meeting_point else None
        random_meeting_point_name = random_meeting_point.nom if random_meeting_point else None
        try:
            accept_rdv, message, ants_response, appointments = get_status_of_predemandes(
                identifiant_predemandes=identifiant_predemandes,
                # attach the status request to a random lieu of the raccordement
                meeting_point_id=random_meeting_point_id,
            )
        except ANTSError as e:
            logger.warning(
                'could not check status of rdv for identifiants_predemandes(%s): %s',
                identifiant_predemandes,
                e,
            )
            return JsonResponse(
                {
                    'err': 1,
                    'data': str(e),
                }
            )
        if accept_rdv:
            logger.info(
                'rdv-status(%s, %s) for application_ids %s is ok.',
                request.raccordement,
                random_meeting_point_name,
                ','.join(identifiant_predemandes),
            )
        else:
            logger.info(
                'rdv-status(%s, %s) for application_ids %s is nok: %s',
                request.raccordement,
                random_meeting_point_name,
                ','.join(identifiant_predemandes),
                message,
            )
        return JsonResponse(
            {
                'err': 0,
                'data': {
                    'accept_rdv': accept_rdv,
                    'message': message,
                    'ants_response': ants_response,
                    'appointments': appointments,
                },
            }
        )


rdv_status = csrf_exempt(authenticate(RdvStatus.as_view()))
