# ANTS-Hub - Copyright (C) Entr'ouvert

import os

DEBUG = True

ALLOWED_HOSTS = ['*']

# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'ants_hub.data',
    'ants_hub.api',
]

MIDDLEWARE = [
    'ants_hub.middleware.profile_middleware',
]

PROFILE_FILE = None

SILENCED_SYSTEM_CHECKS = [
    'admin.E408',
    'admin.E409',
    'admin.E410',
]

ADMIN_MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'ants_hub.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'ants_hub.wsgi.application'

DATABASES = {
    'default': {
        'ENGINE': 'ants_hub.sqlite3_backend',
        'NAME': 'db.sqlite3',
        # Keep database open
        'CONN_MAX_AGE': None,
    }
}
LANGUAGE_CODE = 'fr-fr'
TIME_ZONE = 'Europe/Paris'
USE_I18N = True
USE_L10N = True
USE_TZ = True
STATIC_URL = '/static/'

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'stream': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'stream',
        },
    },
    'formatters': {
        'stream': {
            'format': '%(asctime)s %(levelname)s %(name)s: %(message)s',
            'datefmt': '%Y-%m-%d %a %H:%M:%S',
        },
    },
    'loggers': {
        '': {
            'handlers': [],
            'level': 'DEBUG',
            'propagate': True,
        },
        'ants_hub': {
            'handlers': ['stream'],
            'level': 'DEBUG',
        },
    },
}

# ANTS_HUB_X_HUB_RDV_AUTH_TOKEN
ANTS_HUB_X_HUB_RDV_AUTH_TOKEN = None

# ANTS_HUB_ADMIN_ROLE
ANTS_HUB_ADMIN_ROLE = None

# ANTS_HUB_API_URL
ANTS_HUB_API_URL = 'https://%s:@ants-hub.entrouvert.org/api/chrono/'

ANTS_HUB_BUSY_BACKOFF = 0.5

ANTS_HUB_BASE_URL = 'https://ants-hub.entrouvert.org'


if 'ANTS_HUB_SETTINGS_FILE' in os.environ:
    with open(os.environ['ANTS_HUB_SETTINGS_FILE']) as fd:
        exec(fd.read())
