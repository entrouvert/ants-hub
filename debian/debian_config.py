# This file is sourced by "execfile" from ants_hub.settings

import glob
import os

PROJECT_NAME = 'ants-hub'
ETC_DIR = '/etc/%s' % PROJECT_NAME
VAR_DIR = '/var/lib/%s' % PROJECT_NAME
# collecstatic destination
STATIC_ROOT = os.path.join(VAR_DIR, 'collectstatic')
# media destination
MEDIA_ROOT = os.path.join(VAR_DIR, 'media')

DATABASES = {
    'default': {
        'ENGINE': 'ants_hub.sqlite3_backend',
        'NAME': os.path.join(VAR_DIR, 'db.sqlite3'),
    }
}

ADMINS = (('ANTS-Hub Administrator', 'root@localhost'),)
EMAIL_SUBJECT_PREFIX = '[%s] ' % PROJECT_NAME

LOGGING = {
    'version': 1,
    'formatters': {
        'syslog': {
            'format': '%(levelname)s %(name)s %(message)s',
        },
    },
    'handlers': {
        'syslog': {
            'level': 'DEBUG',
            'address': '/dev/log',
            'class': 'logging.handlers.SysLogHandler',
            'formatter': 'syslog',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
        },
    },
    'loggers': {
        '': {
            'level': 'ERROR',
            'handlers': ['mail_admins', 'syslog'],
        },
        'django': {
            'level': 'NOTSET',
            'handlers': [],
            'propagate': True,
        },
        'ants_hub': {
            'level': 'INFO',
            'propagate': True,
        },
    },
}

# Journald support
if os.path.exists('/run/systemd/journal/socket'):
    try:
        from systemd import journal
    except ImportError:
        pass
    else:
        LOGGING['handlers']['journald'] = {
            'level': 'INFO',
            'class': 'hobo.journal.JournalHandler',
            'formatter': 'syslog',
        }
        LOGGING['loggers']['']['handlers'].remove('syslog')
        LOGGING['loggers']['']['handlers'].append('journald')

# HTTPS Security
CSRF_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True
CSRF_COOKIE_SAMESITE = 'Strict'
SESSION_COOKIE_SECURE = True
SESSION_COOKIE_SAMESITE = 'Strict'
SESSION_COOKIE_HTTPONLY = True

with open(os.path.join(VAR_DIR, 'secret')) as fd:
    SECRET_KEY = fd.read()

main_settings_py = os.path.join(ETC_DIR, 'settings.py')

for filename in [main_settings_py] + sorted(glob.glob(os.path.join(ETC_DIR, 'settings.d', '*.py'))):
    with open(filename) as fd:
        exec(fd.read())
