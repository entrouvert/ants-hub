# SECURITY WARNING: don't run with debug turned on in production!
# DEBUG = False
# TEMPLATE_DEBUG = False

# ADMINS = (
#     ('ANTS-Hub Administrator', 'root@localhost'),
# )

# ALLOWED_HOSTS must be correct in production!
# See https://docs.djangoproject.com/en/dev/ref/settings/#allowed-hosts
# ALLOWED_HOSTS = [
#     '*',
# ]

# Databases
# DATABASES = {
#     'default': {
#         'ENGINE': 'ants_hub.sqlite3_backend',
#         'NAME': os.path.join(VAR_DIR, 'db.sqlite3'),
#     }
# }

# EMAIL_SUBJECT_PREFIX = '[ants-hub] '

# LANGUAGE_CODE = 'fr-fr'
# TIME_ZONE = 'Europe/Paris'

# Email configuration
# EMAIL_SUBJECT_PREFIX = '[ants-hub] '
# SERVER_EMAIL = 'root@ants-hub.example.org'
# DEFAULT_FROM_EMAIL = 'webmaster@ants-hub.example.org'

# SMTP configuration
# EMAIL_HOST = 'localhost'
# EMAIL_HOST_USER = ''
# EMAIL_HOST_PASSWORD = ''
# EMAIL_PORT = 25

# HTTPS Security
# CSRF_COOKIE_SECURE = True
# CSRF_COOKIE_HTTPONLY = True
# CSRF_COOKIE_SAMESITE = 'Strict'
# SESSION_COOKIE_SECURE = True
# SESSION_COOKIE_SAMESITE = 'Strict'
# SESSION_COOKIE_HTTPONLY = True
